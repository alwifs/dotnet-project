﻿using Med_Id.datamodels;
using Med_Id.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace Med_Id.web.Services
{
	public class AuthService
	{
		private static readonly HttpClient client = new HttpClient();
		private IConfiguration configuration;
		private string RouteAPI = "";
		private VMResponse respon = new VMResponse();

		//cara dapetin value dari appsetting json
		public AuthService(IConfiguration _configuration)
		{
			this.configuration = _configuration;
			this.RouteAPI = this.configuration["RouteAPI"];
		}

		public async Task<List<VMMenu>> MenuAccess(int IdRole)
		{
			List<VMMenu> data = new List<VMMenu>();

			string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAuth/MenuAccess/{IdRole}");
			data = JsonConvert.DeserializeObject<List<VMMenu>>(apiResponse)!;
				
			return data;
		}

	}
}
