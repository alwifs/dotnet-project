﻿using Med_Id.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace Med_Id.web.Services
{
    public class CourierTypeService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public CourierTypeService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<List<VMCourierType>> GetAllData()
        {
            List<VMCourierType> data = new List<VMCourierType>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiCourierType/GetAllData");

            data = JsonConvert.DeserializeObject<List<VMCourierType>>(apiResponse)!;

            return data;
        }
        public async Task<VMResponse> Create(VMCourierType dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PostAsync(RouteAPI + "apiCourierType/Save", content);


            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<VMCourierType> GetDataById(int id)
        {
            VMCourierType data = new VMCourierType();
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiCourierType/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMCourierType>(apiRespon)!;

            return data;

        }
        public async Task<List<VMCourierType>> GetDataByCourierId(int id)
        {
            List<VMCourierType> data = new List<VMCourierType>();
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiCourierType/GetDataByCourierId/{id}");
            data = JsonConvert.DeserializeObject<List<VMCourierType>>(apiRespon)!;

            return data;

        }

        public async Task<VMResponse> Edit(VMCourierType dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PutAsync(RouteAPI + "apiCourierType/Edit", content);


            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<VMResponse> Detail(VMCourierType dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PutAsync(RouteAPI + "apiCourierType/Detail", content);


            if (request.IsSuccessStatusCode)
            {   
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<VMResponse> Delete(int id, int DeletedBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCourierType/Delete/{id}/{DeletedBy}");

            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
        

    }
}
