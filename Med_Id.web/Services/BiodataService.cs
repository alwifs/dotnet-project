﻿using Med_Id.datamodels;
using Med_Id.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace Med_Id.web.Services
{
    public class BiodataService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public BiodataService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<MBiodatum>> GetAllData()
        {
            List<MBiodatum> data = new List<MBiodatum>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiBiodata/GetAllData");

            data = JsonConvert.DeserializeObject<List<MBiodatum>>(apiResponse)!;

            return data;
        }
        public async Task<VMResponse> Create(MBiodatum dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PostAsync(RouteAPI + "apiBiodata/Save", content);


            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<MBiodatum> GetDataById(int id)
        {
            MBiodatum data = new MBiodatum();
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiBiodata/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<MBiodatum>(apiRespon)!;

            return data;
        }

        public async Task<VMResponse> Edit(MBiodatum dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PutAsync(RouteAPI + "apiBiodata/Edit", content);


            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id, int DeletedBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiBiodata/Delete/{id}/{DeletedBy}");

            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
        public async Task<VMResponse> Detail(MBiodatum dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PutAsync(RouteAPI + "apiBiodata/Detail", content);


            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
    }
}
