﻿using Med_Id.datamodels;
using Med_Id.viewmodels;
using Med_Id.web.Services;
using Microsoft.AspNetCore.Mvc;

namespace Med_Id.web.Controllers
{
    public class CourierTypeController : Controller
    {
        private CourierService courierService;
        private CourierTypeService courierTypeService;
        private int IdUser = 1;

        public CourierTypeController(CourierService _courierService, CourierTypeService _courierTypeService)
        {

            this.courierService = _courierService;
            this.courierTypeService = _courierTypeService;

        }
        public async Task<IActionResult> Index(string sortOrder,
                                                string searchString,
                                                string currentFilter,
                                                int? pageNumber,
                                                int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<VMCourierType> data = await courierTypeService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name.ToLower().Contains(searchString.ToLower())
                ).ToList();
            }
            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    break;
            }
            return View(PaginationList<VMCourierType>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }
        public async Task<IActionResult> Create()
        {
            VMCourierType data = new VMCourierType();

            List<MCourier> listCourier = await courierService.GetAllData();
            ViewBag.ListCourier = listCourier;

            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(VMCourierType dataParam)
        {
            VMResponse respon = await courierTypeService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }
        public async Task<IActionResult> Edit(int id)
        {
            VMCourierType data = await courierTypeService.GetDataById(id);

            List<MCourier> listCourier = await courierService.GetAllData();
            ViewBag.ListCourier = listCourier;

            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(VMCourierType dataParam)
        {
            VMResponse respon = await courierTypeService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            VMCourierType data = await courierTypeService.GetDataById(id);

            List<MCourier> listCourier = await courierService.GetAllData();
            ViewBag.ListCourier = listCourier;

            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Detail(VMCourierType dataParam)
        {
            VMResponse respon = await courierTypeService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }
        public async Task<IActionResult> Delete(int id)
        {
            VMCourierType data = await courierTypeService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> SureDelete(int id, int DeletedBy)
        {

            VMResponse respon = await courierTypeService.Delete(id, DeletedBy);

            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", id);
        }
    }
}
