﻿using Med_Id.datamodels;
using Med_Id.viewmodels;
using Med_Id.web.Services;
using Microsoft.AspNetCore.Mvc;

namespace Med_Id.web.Controllers
{
    public class BiodataController : Controller
    {
        private BiodataService biodataService;
        private int IdUser = 1;

        public BiodataController(BiodataService _biodataService)
        {
            this.biodataService = _biodataService;

        }
        public async Task<IActionResult> Index(string sortOrder,
                                                string searchString,
                                                string currentFilter,
                                                int? pageNumber,
                                                int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<MBiodatum> data = await biodataService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Fullname.ToLower().Contains(searchString.ToLower())
                ).ToList();
            }
            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Fullname).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Fullname).ToList();
                    break;
            }
            return View(PaginationList<MBiodatum>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public IActionResult Create()
        {
            MBiodatum data = new MBiodatum();
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(MBiodatum dataParam)
        {
            VMResponse respon = await biodataService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            MBiodatum data = await biodataService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(MBiodatum dataParam)
        {
            VMResponse respon = await biodataService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }
        public async Task<IActionResult> Delete(int id)
        {
            MBiodatum data = await biodataService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> SureDelete(int id, int DeletedBy)
        {
            DeletedBy = IdUser;
            VMResponse respon = await biodataService.Delete(id, DeletedBy);

            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", id);
        }
        public async Task<IActionResult> Detail(int id)
        {
            MBiodatum data = await biodataService.GetDataById(id);
            return PartialView(data);
        }
    }
}
