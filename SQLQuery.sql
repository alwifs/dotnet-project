use [med.id]

--
truncate table m_user
select * from m_user
insert into m_user values	
(1, 1, 'admin@gmail.com', 1234, 0,0, getdate(), 1, getdate(), null, null, null, null,0),
(2, 2, 'dokter@gmail.com', 1234, 0,0, getdate(), 1, getdate(), null, null, null, null,0),
(3, 3, 'faskes@gmail.com', 1234, 0,0, getdate(), 1, getdate(), null, null, null, null,0),
(4, 4, 'pasien@gmail.com', 1234, 0,0, getdate(), 1, getdate(), null, null, null, null,0),
(5, 5, 'umum@gmail.com', 1234, 0,0, getdate(), 1, getdate(), null, null, null, null,0)
--
select * from m_role

select * from m_menu
select * from m_menu_role


--
--truncate table m_menu
select * from m_menu


truncate table m_menu
insert into m_menu values
('Master', '', 0, 'nav-icon fas fa-light', 'nav-icon fas fa-light' ,1, getdate(), null, null, null, null, 0),
('Transaction', '', 0, 'nav-icon fas fa-light', 'nav-icon fas fa-light' ,1, getdate(), null, null, null, null, 0),
('Home', 'Home', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Admin', 'Admin', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon', 1, getdate(), null, null, null, null, 0),
('Bank', 'Bank', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Biodata', 'Biodata', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('BiodataAddress' , 'BiodataAddress', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('BloodGroup', 'BloodGroup', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Courier', 'Courier', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CourierType', 'CourierType', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CourierDiscount' , 'CourierDiscount', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Customer', 'Customer', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerMember', 'CustomerMember', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerRelation' , 'CustomerRelation', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerChat', 'CustomerChat', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' , 1, getdate(), null, null, null, null, 0),
('CustomerChatHistory','CustomerChatHistory', 1,'far fa-light fa-file nav-icon','far fa-light fa-file nav-icon',1, getdate(), null, null, null, null,0),
('CustCustNominal', 'CustCustNominal', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerRegisCard', 'CustomerRegisCard', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerVA', 'CustomerVA', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerVAHist', 'CustomerVAHist', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon', 1, getdate(), null, null, null, null, 0),
('CustomerWallet', 'CustomerWallet', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerWallTP', 'CustomerWallTP', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerWallWD', 'CustomerWallWD', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Doctor','Doctor', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon', 1, getdate(), null, null, null, null, 0),
('DoctorEducation', 'DoctorEducation', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('DoctorTreatment', 'DoctorTreatment', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('DoctorOffice', 'DoctorOffice', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('DoctorOfficeSche', 'DoctorOfficeSche', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon', 1, getdate(), null, null, null, null, 0),
('DoctorOfficeTreat', 'DoctorOfficeTreat', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('DoctorOfcTreatPrice', 'DoctorOfcTreatPrice', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CurrentDoctorSpez', 'CurrentDoctorSpez', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('EducationLevel' , 'EducationLevel', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Location', 'Location', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('LocationLevel' , 'LocationLevel', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedicalFacility', 'MedicalFacility', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedFacilityCatg', 'MedFacilityCatg', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedFacilitySch', 'MedFacilitySch', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedicalItem', 'MedicalItem', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedItemCatg', 'MedItemCatg', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedItemSegmen', 'MedItemSegmen', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedlItemPurch', 'MedlItemPurch', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedItemPurchDet', 'MedItemPurchDet', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Menu', 'Menu', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MenuRole', 'MenuRole', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('PaymentMethod', 'PaymentMethod', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Role', 'Role', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Spezialization', 'Spezialization', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('User', 'User', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Wallet', 'Wallet', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('BioAttachment', 'BioAttachment', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Appointment', 'Appointment', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('AppointmentResch', 'AppointmentResch', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('TreatmentDisc' , 'TreatmentDisc', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0)
--

update m_menu set url = 'Index' where Id > 2

select * from m_menu as parent
join m_menu_role as mr on parent.Id = mr.menu_Id
where mr.role_Id = 1 and parent.parent_id = 0

select * from m_menu as parent
join m_menu_role as mr on parent.Id = mr.menu_Id
where parent.parent_id = 0 and parent.is_delete=0 and mr.is_delete=0 and mr.role_Id=2


select * from m_menu as parent
join m_menu_role as mr on parent.Id = mr.menu_Id
where parent.parent_id = 1 and parent.is_delete=0 and mr.is_delete=0 and mr.role_Id=2


--truncate table m_menu_role
select * from m_menu_role
select * from m_menu
select * from m_role

truncate table m_menu_role
insert into m_menu_role values
-- Menu Admin
(1,1, 1, getdate(), null, null, null, null, 0), -- Menu Home/ Landingpage awal
(2,1, 1, getdate(), null, null, null, null, 0), -- Menu Admin
(3,1, 1, getdate(), null, null, null, null, 0), -- Menu Bank
(4,1, 1, getdate(), null, null, null, null, 0), -- Menu Biodata/ Profil
(5,1, 1, getdate(), null, null, null, null, 0), -- Menu Biodata/ Profil
(6,1, 1, getdate(), null, null, null, null, 0), -- Menu
(7,1, 1, getdate(), null, null, null, null, 0), -- Menu
(8,1, 1, getdate(), null, null, null, null, 0), -- Menu
(9,1, 1, getdate(), null, null, null, null, 0), -- Menu
(10,1, 1, getdate(), null, null, null, null, 0), -- Menu
(11,1, 1, getdate(), null, null, null, null, 0), -- Menu
(12,1, 1, getdate(), null, null, null, null, 0), -- Menu
(13,1, 1, getdate(), null, null, null, null, 0), -- Menu
(14,1, 1, getdate(), null, null, null, null, 0), -- Menu
(15,1, 1, getdate(), null, null, null, null, 0), -- Menu
(16,1, 1, getdate(), null, null, null, null, 0), -- Menu
(17,1, 1, getdate(), null, null, null, null, 0), -- Menu
(18,1, 1, getdate(), null, null, null, null, 0), -- Menu
(19,1, 1, getdate(), null, null, null, null, 0), -- Menu
(20,1, 1, getdate(), null, null, null, null, 0), -- Menu
(21,1, 1, getdate(), null, null, null, null, 0), -- Menu
(22,1, 1, getdate(), null, null, null, null, 0), -- Menu
(23,1, 1, getdate(), null, null, null, null, 0), -- Menu
(24,1, 1, getdate(), null, null, null, null, 0), -- Menu
(25,1, 1, getdate(), null, null, null, null, 0), -- Menu
(26,1, 1, getdate(), null, null, null, null, 0), -- Menu
(27,1, 1, getdate(), null, null, null, null, 0), -- Menu
(28,1, 1, getdate(), null, null, null, null, 0), -- Menu
(29,1, 1, getdate(), null, null, null, null, 0), -- Menu
(30,1, 1, getdate(), null, null, null, null, 0), -- Menu
(31,1, 1, getdate(), null, null, null, null, 0), -- Menu
(32,1, 1, getdate(), null, null, null, null, 0), -- Menu
(33,1, 1, getdate(), null, null, null, null, 0), -- Menu
(34,1, 1, getdate(), null, null, null, null, 0), -- Menu
(35,1, 1, getdate(), null, null, null, null, 0), -- Menu
(36,1, 1, getdate(), null, null, null, null, 0), -- Menu
(37,1, 1, getdate(), null, null, null, null, 0), -- Menu
(38,1, 1, getdate(), null, null, null, null, 0), -- Menu
(39,1, 1, getdate(), null, null, null, null, 0), -- Menu
(40,1, 1, getdate(), null, null, null, null, 0), -- Menu
(41,1, 1, getdate(), null, null, null, null, 0), -- Menu
(42,1, 1, getdate(), null, null, null, null, 0), -- Menu
(43,1, 1, getdate(), null, null, null, null, 0), -- Menu
(44,1, 1, getdate(), null, null, null, null, 0), -- Menu
(45,1, 1, getdate(), null, null, null, null, 0), -- Menu
(46,1, 1, getdate(), null, null, null, null, 0), -- Menu
(47,1, 1, getdate(), null, null, null, null, 0), -- Menu
(48,1, 1, getdate(), null, null, null, null, 0), -- Menu
(49,1, 1, getdate(), null, null, null, null, 0), -- Menu
(50,1, 1, getdate(), null, null, null, null, 0), -- Menu
(51,1, 1, getdate(), null, null, null, null, 0), -- Menu
(52,1, 1, getdate(), null, null, null, null, 0), -- Menu
(53,1, 1, getdate(), null, null, null, null, 0), -- Menu

--Menu Doctor
(1,2, 1, getdate(), null, null, null, null, 0),
(3,2, 1, getdate(), null, null, null, null, 0), -- Menu Landing Page/ home
(6,2, 1, getdate(), null, null, null, null, 0), -- Menu Profil/ Biodata
(24,2, 1, getdate(), null, null, null, null, 0), -- Menu Dokter
(31,2, 1, getdate(), null, null, null, null, 0), -- Menu Tambah Dokter Spesialis
(47,2, 1, getdate(), null, null, null, null, 0), -- Menu Spesialis Dokter

--Menu Faskes
(6,3, 1, getdate(), null, null, null, null, 0), -- Menu Profil/ Biodata
(3,3, 1, getdate(), null, null, null, null, 0), -- Menu Landing Page/ home
(9,3, 1, getdate(), null, null, null, null, 0), -- Menu Courier
(10,3, 1, getdate(), null, null, null, null, 0), -- Menu Courier
(1,3, 1, getdate(), null, null, null, null, 0), -- Menu


--Menu Pasien
(1,4, 1, getdate(), null, null, null, null, 0), -- Menu
(3,4, 1, getdate(), null, null, null, null, 0), -- Menu Landing Page/ home
(5,4, 1, getdate(), null, null, null, null, 0), -- Menu Biodata/ Profil
(6,4, 1, getdate(), null, null, null, null, 0), -- Menu Profil/ Biodata
(7,4, 1, getdate(), null, null, null, null, 0), -- Menu Tab Alamat
(12,4, 1, getdate(), null, null, null, null, 0), -- Menu Pasien
(24,4, 1, getdate(), null, null, null, null, 0), -- Menu Cari Dokter/ Detail
(37,4, 1, getdate(), null, null, null, null, 0), -- Menu Riwayat Kedatangan
(38,4, 1, getdate(), null, null, null, null, 0), -- Menu Produk  Kesehatan
(50,4, 1, getdate(), null, null, null, null, 0), -- Menu Tab Profil
(51,4, 1, getdate(), null, null, null, null, 0) -- Menu Buat Janji




-- Menu Umum 
--(3,5, getdate(), 1, getdate(), 1, getdate(), 0)
--(24,5, getdate(), 1, getdate(), 1, getdate(), 0)


select * from m_biodata
truncate table m_biodata
insert into m_biodata values	
('dr. Tatjana Saphira, Sp.A', '081234567890', null,null, 1, getdate(), null, null, null, null,0),
('dr. Alwi Fadli, Sr.G', '081234567890', null,null, 1, getdate(), null, null, null, null,0),
('dr. Toni Agustina, Sp.Farm', '081234567890', null,null, 1, getdate(), null, null, null, null,0)

truncate table m_doctor
select * from m_doctor
insert into m_doctor values	
(1, 'DR001', 1, getdate(), null, null, null, null,0),
(2, 'DR002', 1, getdate(), null, null, null, null,0),
(3, 'DR003', 1, getdate(), null, null, null, null,0)

select * from m_biodata
-- Join Table JadwaL Praktek
select * from m_medical_facility -- RS Fasilitas -- Lokasi Praktek
select * from m_medical_facility_schedule -- Jadwal
select * from m_location -- Lokasi 
select * from m_medical_facility_category -- Jenis Poli
select * from t_doctor_office_treatment_price -- Biaya treatmen
select * from t_doctor_office_schedule -- Schedule


-- Join Table Riwayat Praktek
select * from m_medical_facility -- RS Fasilitas -- Lokasi Praktek
select * from m_location -- Lokasi 
select * from m_specialization -- Spesilis 
select * from m_doctor
select * from t_current_doctor_specialization

--Quwery
select mdr.name as Name_RS, loc.name as Name_Loc
from m_medical_facility as mdr
join m_location as loc on mdr.location_id = loc.id

-- Query Riwayat Doctor
select bio.fullname as NamaDoctor, spe.name as NamaSpesialis, mdfc.name as NamaRS, loc.name as LocName
from t_current_doctor_specialization as dsp
join m_specialization as spe on spe.Id = dsp.specialization_id
join m_doctor as doc on doc.Id = dsp.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id
join t_doctor_office_schedule as drsc on doc.id = drsc.doctor_id
join m_medical_facility_schedule as mdfs on drsc.medical_facility_schedule_id = mdfs.id
join m_medical_facility as mdfc on mdfs.medical_facility_id = mdfc.id
join m_location as loc on mdfc.location_id = loc.id

-- Query Riwayat Doctor
select bio.fullname as NamaDoctor, spe.name as NamaSpesialis, mdfc.name as NamaRS,
mdfs.day as Hari, mdfs.time_schedule_start as Mulai, mdfs.time_schedule_end as Selesai
from t_current_doctor_specialization as dsp
join m_specialization as spe on spe.Id = dsp.specialization_id
join m_doctor as doc on doc.Id = dsp.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id
join t_doctor_office_schedule as drsc on doc.id = drsc.doctor_id
join m_medical_facility_schedule as mdfs on drsc.medical_facility_schedule_id = mdfs.id
join m_medical_facility as mdfc on mdfs.medical_facility_id = mdfc.id
join m_location as loc on mdfc.location_id = loc.id


-- Query Lokasi Doctor
select distinct bio.fullname as NamaDoctor, medfac.name as NamaPoli, mdfc.name as NamaRS, 
mdfs.day as Hari, mdfs.time_schedule_start as Mulai, mdfs.time_schedule_end as Selesai,
dotp.price_start_from as HargaMulai
from t_current_doctor_specialization as dsp
join m_doctor as doc on doc.Id = dsp.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id
join t_doctor_office_schedule as drsc on doc.id = drsc.doctor_id
join m_medical_facility_schedule as mdfs on drsc.medical_facility_schedule_id = mdfs.id
join m_medical_facility as mdfc on mdfs.medical_facility_id = mdfc.id
join m_medical_facility_category as medfac on medfac.id = mdfc.medical_facility_category_id
join t_doctor_office as dco on doc.id = dco.doctor_id
join t_doctor_office_treatment as dot on dot.doctor_office_id = dco.id
join t_doctor_office_treatment_price as dotp on dotp.doctor_office_treatment_id = dot.id

select * from m_medical_facility_category
select * from t_doctor_office

select * from t_doctor_office_treatment
insert into t_doctor_office_treatment values
(1, 1, 1, getdate(), null, null, null, null, 0),
(2, 1, 1, getdate(), null, null, null, null, 0),
(3, 1, 1, getdate(), null, null, null, null, 0),
(4, 1, 1, getdate(), null, null, null, null, 0)


select * from t_doctor_office_treatment_price -- Biaya treatmen
insert into t_doctor_office_treatment_price values
(1, 500000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(2, 600000, 400000, 1000000, 1,  getdate(), null, null, null, null, 0),
(3, 700000, 500000, 1000000, 1,  getdate(), null, null, null, null, 0),
(4, 800000, 600000, 1000000, 1,  getdate(), null, null, null, null, 0)



select * from t_doctor_office_schedule -- Schedule
select * from m_medical_facility_schedule -- Jadwal



select * from m_medical_facility_category -- Jenis Poli
select * from m_medical_facility_schedule -- Jadwal
select * from m_location -- Lokasi 
select * from m_specialization -- Spesilis 

select * from m_doctor
select * from m_doctor_education
select * from m_education_level

select * from m_doctor_education -- Riwayat Pendidikan
select * from t_doctor_treatment -- TIndakan Medis
select * from t_doctor_office_treatment
select * from t_doctor_office_schedule
select * from t_doctor_office_treatment_price
select * from t_doctor_office_schedule
select * from t_current_doctor_specialization
select * from t_appointment
select * from m_biodata_attachment
select * from m_biodata
select * from