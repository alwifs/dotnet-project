﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Med_Id.datamodels
{
    [Table("t_customer_wallet_withdraw")]
    public partial class TCustomerWalletWithdraw
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("customer_id")]
        public long CustomerId { get; set; }
        [Column("wallet_default_nominal_id")]
        public long? WalletDefaultNominalId { get; set; }
        [Column("amount")]
        public int Amount { get; set; }
        [Column("bank_name")]
        [StringLength(50)]
        [Unicode(false)]
        public string BankName { get; set; } = null!;
        [Column("account_number")]
        [StringLength(50)]
        [Unicode(false)]
        public string AccountNumber { get; set; } = null!;
        [Column("acoount_name")]
        [StringLength(255)]
        [Unicode(false)]
        public string AcoountName { get; set; } = null!;
        [Column("otp")]
        public int Otp { get; set; }
        [Column("created_by")]
        public long CreatedBy { get; set; }
        [Column("created_on", TypeName = "datetime")]
        public DateTime CreatedOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("deleted_by")]
        public long? DeletedBy { get; set; }
        [Column("delted_on", TypeName = "datetime")]
        public DateTime? DeltedOn { get; set; }
        [Column("is_delete")]
        public bool IsDelete { get; set; }
    }
}
