﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Med_Id.datamodels
{
    [Table("t_medical_item_purchase_detail")]
    public partial class TMedicalItemPurchaseDetail
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("medical_item_purchase_id")]
        public long? MedicalItemPurchaseId { get; set; }
        [Column("medical_item_item")]
        public long? MedicalItemItem { get; set; }
        [Column("qty")]
        public int? Qty { get; set; }
        [Column("medical_facility_id")]
        public long? MedicalFacilityId { get; set; }
        [Column("courir_id")]
        public long? CourirId { get; set; }
        [Column("sub_total", TypeName = "decimal(18, 0)")]
        public decimal? SubTotal { get; set; }
        [Column("create_by")]
        public long CreateBy { get; set; }
        [Column("create_on", TypeName = "datetime")]
        public DateTime CreateOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("delete_by")]
        public long? DeleteBy { get; set; }
        [Column("delete_on", TypeName = "datetime")]
        public DateTime? DeleteOn { get; set; }
        [Column("is_delete")]
        public bool IsDelete { get; set; }
    }
}
