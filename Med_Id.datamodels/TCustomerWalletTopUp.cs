﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Med_Id.datamodels
{
    [Table("t_customer_wallet_top_up")]
    public partial class TCustomerWalletTopUp
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("customer_wallet_id")]
        public long? CustomerWalletId { get; set; }
        [Column("amount", TypeName = "decimal(18, 0)")]
        public decimal? Amount { get; set; }
        [Column("create_by")]
        public long CreateBy { get; set; }
        [Column("create_on", TypeName = "datetime")]
        public DateTime CreateOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("delete_by")]
        public long? DeleteBy { get; set; }
        [Column("delete_on", TypeName = "datetime")]
        public DateTime? DeleteOn { get; set; }
        [Column("is_delete")]
        public bool IsDelete { get; set; }
    }
}
