USE [master]
GO
/****** Object:  Database [med.id]    Script Date: 28/04/2023 10:03:33 ******/
CREATE DATABASE [med.id]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'med.id', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\med.id.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'med.id_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\med.id_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [med.id] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [med.id].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [med.id] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [med.id] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [med.id] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [med.id] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [med.id] SET ARITHABORT OFF 
GO
ALTER DATABASE [med.id] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [med.id] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [med.id] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [med.id] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [med.id] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [med.id] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [med.id] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [med.id] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [med.id] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [med.id] SET  ENABLE_BROKER 
GO
ALTER DATABASE [med.id] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [med.id] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [med.id] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [med.id] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [med.id] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [med.id] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [med.id] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [med.id] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [med.id] SET  MULTI_USER 
GO
ALTER DATABASE [med.id] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [med.id] SET DB_CHAINING OFF 
GO
ALTER DATABASE [med.id] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [med.id] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [med.id] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [med.id] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [med.id] SET QUERY_STORE = ON
GO
ALTER DATABASE [med.id] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [med.id]
GO
/****** Object:  Table [dbo].[m_admin]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_admin](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[biodata_id] [bigint] NULL,
	[code] [varchar](10) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_bank]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_bank](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
	[va_code] [varchar](10) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_biodata]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_biodata](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[fullname] [varchar](255) NULL,
	[mobile_phone] [varchar](15) NULL,
	[image] [varbinary](max) NULL,
	[image_path] [varchar](255) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_biodata_address]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_biodata_address](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[biodata_id] [bigint] NULL,
	[label] [varchar](100) NULL,
	[recipient] [varchar](100) NULL,
	[recipient_phone_number] [varchar](15) NULL,
	[location_id] [bigint] NULL,
	[postal_code] [varchar](10) NULL,
	[address] [text] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_biodata_attachment]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_biodata_attachment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[biodata_id] [bigint] NULL,
	[file_name] [varchar](50) NULL,
	[file_path] [varchar](100) NULL,
	[file_size] [int] NULL,
	[file] [varbinary](max) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_blood_group]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_blood_group](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[code] [varchar](5) NULL,
	[description] [varchar](255) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_courier]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_courier](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_courier_type]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_courier_type](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[courier_id] [bigint] NULL,
	[name] [varchar](20) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_customer]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_customer](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[biodata_id] [bigint] NULL,
	[dob] [date] NULL,
	[gender] [varchar](1) NULL,
	[blood_group_id] [bigint] NULL,
	[rhesus_type] [varchar](5) NULL,
	[height] [decimal](18, 0) NULL,
	[weight] [decimal](18, 0) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_customer_member]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_customer_member](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[parent_biodata_id] [bigint] NULL,
	[customer_id] [bigint] NULL,
	[customer_relation_id] [bigint] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_customer_relation]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_customer_relation](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_doctor]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_doctor](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[biodata_id] [bigint] NULL,
	[str] [varchar](50) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_doctor_education]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_doctor_education](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[doctor_id] [bigint] NULL,
	[education_level_id] [bigint] NULL,
	[institution_name] [varchar](100) NULL,
	[major] [varchar](100) NULL,
	[start_year] [varchar](4) NULL,
	[end_year] [varchar](4) NULL,
	[is_last_education] [bit] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_education_level]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_education_level](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](10) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_location]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_location](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
	[parent_id] [bigint] NULL,
	[location_level_id] [bigint] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_location_level]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_location_level](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[abbreviation] [varchar](50) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_medical_facility]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_medical_facility](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[medical_facility_category_id] [bigint] NULL,
	[location_id] [bigint] NULL,
	[full_address] [text] NULL,
	[email] [varchar](100) NULL,
	[phone_code] [varchar](10) NULL,
	[phone] [varchar](15) NULL,
	[fax] [varchar](15) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_medical_facility_category]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_medical_facility_category](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_medical_facility_schedule]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_medical_facility_schedule](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[medical_facility_id] [bigint] NULL,
	[day] [varchar](10) NULL,
	[time_schedule_start] [varchar](10) NULL,
	[time_schedule_end] [varchar](10) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_medical_item]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_medical_item](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[medical_item_category_id] [bigint] NULL,
	[composition] [text] NULL,
	[medical_item_segmentation_id] [bigint] NULL,
	[manufacturer] [varchar](100) NULL,
	[indication] [text] NULL,
	[dosage] [text] NULL,
	[directions] [text] NULL,
	[contraindication] [text] NULL,
	[caution] [text] NULL,
	[packaging] [varchar](50) NULL,
	[price_max] [bigint] NULL,
	[price_min] [bigint] NULL,
	[image] [varbinary](max) NULL,
	[image_path] [varchar](100) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_medical_item_category]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_medical_item_category](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_medical_item_segmentation]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_medical_item_segmentation](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_menu]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_menu](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](20) NULL,
	[url] [varchar](50) NULL,
	[parent_id] [bigint] NULL,
	[big_icon] [varchar](100) NULL,
	[small_icon] [varchar](100) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_menu_role]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_menu_role](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[menu_Id] [bigint] NULL,
	[role_Id] [bigint] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_payment_method]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_payment_method](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_role]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_role](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](20) NULL,
	[code] [varchar](20) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_specialization]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_specialization](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_user]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_user](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[biodata_id] [bigint] NULL,
	[role_id] [bigint] NULL,
	[email] [varchar](100) NULL,
	[password] [varchar](255) NULL,
	[login_attempt] [int] NULL,
	[is_locked] [bit] NULL,
	[last_login] [datetime] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_wallet_default_nominal]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_wallet_default_nominal](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[nominal] [int] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_appointment]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_appointment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[customer_id] [bigint] NULL,
	[doctor_office_id] [bigint] NULL,
	[doctor_office_schedule_id] [bigint] NULL,
	[doctor_office_treatment_id] [bigint] NULL,
	[appointment_date] [date] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_appointment_cancellation]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_appointment_cancellation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[appointment_id] [bigint] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_appointment_done]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_appointment_done](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[appointment_id] [bigint] NULL,
	[diagnosis] [varchar](1) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_appointment_reschedule_history]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_appointment_reschedule_history](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[appointment_id] [bigint] NULL,
	[doctor_office_schedule_id] [bigint] NULL,
	[doctor_office_treatment_id ] [bigint] NULL,
	[appointment_date] [datetime] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_courier_discount]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_courier_discount](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[courier_type-id] [bigint] NULL,
	[value] [decimal](18, 0) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_current_doctor_specialization]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_current_doctor_specialization](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[doctor_id] [bigint] NULL,
	[specialization_id] [bigint] NOT NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_customer_chat]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_customer_chat](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[customer_id] [bigint] NULL,
	[doctor_id] [bigint] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_customer_chat_history]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_customer_chat_history](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[customer_chat_id] [bigint] NULL,
	[chat_content] [text] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_customer_custom_nominal]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_customer_custom_nominal](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[customer_id] [bigint] NULL,
	[nominal] [bigint] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_customer_registered_card]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_customer_registered_card](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[customer_id] [bigint] NULL,
	[card_number] [varchar](20) NULL,
	[validity_period] [date] NULL,
	[cvv] [varchar](5) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_customer_va]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_customer_va](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[customer_id] [bigint] NULL,
	[card_number] [varchar](30) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_customer_va_history]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_customer_va_history](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[customer_va_id] [bigint] NULL,
	[amount] [decimal](18, 0) NULL,
	[expired_on] [datetime] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_customer_wallet]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_customer_wallet](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[customer_id] [bigint] NULL,
	[pin] [varchar](1) NULL,
	[balance] [decimal](18, 0) NULL,
	[barcode] [varchar](1) NULL,
	[points] [decimal](18, 0) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_customer_wallet_top_up]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_customer_wallet_top_up](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[customer_wallet_id] [bigint] NULL,
	[amount] [decimal](18, 0) NULL,
	[create_by] [bigint] NOT NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[delete_by] [bigint] NULL,
	[delete_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_customer_wallet_withdraw]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_customer_wallet_withdraw](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[customer_id] [bigint] NOT NULL,
	[wallet_default_nominal_id] [bigint] NULL,
	[amount] [int] NOT NULL,
	[bank_name] [varchar](50) NOT NULL,
	[account_number] [varchar](50) NOT NULL,
	[acoount_name] [varchar](255) NOT NULL,
	[otp] [int] NOT NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_doctor_office]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_doctor_office](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[doctor_id] [bigint] NULL,
	[medical_facility_id] [bigint] NULL,
	[specialization] [varchar](100) NULL,
	[create_by] [bigint] NOT NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[delete_by] [bigint] NULL,
	[delete_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_doctor_office_schedule]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_doctor_office_schedule](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[doctor_id] [bigint] NULL,
	[medical_facility_schedule_id] [bigint] NULL,
	[slot] [int] NULL,
	[create_by] [bigint] NOT NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[delete_by] [bigint] NULL,
	[delete_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_doctor_office_treatment]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_doctor_office_treatment](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[doctor_treatment_id] [bigint] NULL,
	[doctor_office_id] [bigint] NULL,
	[create_by] [bigint] NOT NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[delete_by] [bigint] NULL,
	[delete_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_doctor_office_treatment_price]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_doctor_office_treatment_price](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[doctor_office_treatment_id] [bigint] NULL,
	[price] [decimal](18, 0) NULL,
	[price_start_from] [decimal](18, 0) NULL,
	[price_until_from] [decimal](18, 0) NULL,
	[create_by] [bigint] NOT NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[delete_by] [bigint] NULL,
	[delete_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_doctor_treatment]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_doctor_treatment](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[doctor_id] [bigint] NULL,
	[name] [varchar](50) NULL,
	[create_by] [bigint] NOT NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[delete_by] [bigint] NULL,
	[delete_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_medical_item_purchase]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_medical_item_purchase](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[customer_id] [bigint] NULL,
	[payment_method_id] [bigint] NULL,
	[create_by] [bigint] NOT NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[delete_by] [bigint] NULL,
	[delete_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_medical_item_purchase_detail]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_medical_item_purchase_detail](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[medical_item_purchase_id] [bigint] NULL,
	[medical_item_item] [bigint] NULL,
	[qty] [int] NULL,
	[medical_facility_id] [bigint] NULL,
	[courir_id] [bigint] NULL,
	[sub_total] [decimal](18, 0) NULL,
	[create_by] [bigint] NOT NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[delete_by] [bigint] NULL,
	[delete_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_prescription]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_prescription](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[appointment_id] [bigint] NULL,
	[medical_item_id] [bigint] NULL,
	[dosage] [text] NULL,
	[direction] [text] NULL,
	[time] [varchar](100) NULL,
	[notes] [text] NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_reset_password]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_reset_password](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[old_password] [varchar](255) NULL,
	[new_password] [varchar](255) NULL,
	[reset_for] [varchar](20) NULL,
	[create_by] [bigint] NOT NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[delete_by] [bigint] NULL,
	[delete_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_token]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_token](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[email] [varchar](100) NULL,
	[user_id] [bigint] NULL,
	[token] [varchar](50) NULL,
	[expired_on] [datetime] NULL,
	[is_expired] [bit] NULL,
	[used_for] [varchar](20) NULL,
	[create_by] [bigint] NOT NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[delete_by] [bigint] NULL,
	[delete_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_treatment_discount]    Script Date: 28/04/2023 10:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_treatment_discount](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[doctor_office_treatment_price_id] [bigint] NULL,
	[value] [decimal](18, 0) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[m_biodata] ON 

INSERT [dbo].[m_biodata] ([id], [fullname], [mobile_phone], [image], [image_path], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (1, N'dr. Tatjana Saphira, Sp.A', N'081234567890', NULL, NULL, 1, CAST(N'2023-04-26T14:47:14.117' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_biodata] ([id], [fullname], [mobile_phone], [image], [image_path], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (2, N'dr. Alwi Fadli, Sr.G', N'081234567890', NULL, NULL, 1, CAST(N'2023-04-26T14:47:14.117' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_biodata] ([id], [fullname], [mobile_phone], [image], [image_path], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (3, N'dr. Toni Agustina, Sp.Farm', N'081234567890', NULL, NULL, 1, CAST(N'2023-04-26T14:47:14.117' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_biodata] OFF
GO
SET IDENTITY_INSERT [dbo].[m_biodata_address] ON 

INSERT [dbo].[m_biodata_address] ([id], [biodata_id], [label], [recipient], [recipient_phone_number], [location_id], [postal_code], [address], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (1, 1, N'Dokter', NULL, NULL, 1, NULL, NULL, 1, CAST(N'2023-01-24T20:29:37.353' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_biodata_address] ([id], [biodata_id], [label], [recipient], [recipient_phone_number], [location_id], [postal_code], [address], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (2, 2, N'Dokter', NULL, NULL, 1, NULL, NULL, 1, CAST(N'2023-01-24T20:29:37.353' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_biodata_address] OFF
GO
SET IDENTITY_INSERT [dbo].[m_blood_group] ON 

INSERT [dbo].[m_blood_group] ([id], [code], [description], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (1, N'A', N'-', 1, CAST(N'2023-01-20T09:18:46.850' AS DateTime), NULL, NULL, 1, CAST(N'2023-01-20T14:25:59.803' AS DateTime), 0)
INSERT [dbo].[m_blood_group] ([id], [code], [description], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (2, N'B', N'-', 1, CAST(N'2023-01-20T09:36:33.877' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_blood_group] ([id], [code], [description], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (3, N'O', N'-', 1, CAST(N'2023-01-20T10:00:01.297' AS DateTime), NULL, NULL, 1, CAST(N'2023-01-20T14:28:18.560' AS DateTime), 0)
INSERT [dbo].[m_blood_group] ([id], [code], [description], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (4, N'AB', N'+', 1, CAST(N'2023-01-20T10:04:48.510' AS DateTime), 1, CAST(N'2023-02-02T10:52:25.953' AS DateTime), 1, CAST(N'2023-01-20T14:27:28.103' AS DateTime), 0)
INSERT [dbo].[m_blood_group] ([id], [code], [description], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (5, N'Biru', N'Bangsawan Kerajaan', 1, CAST(N'2023-01-20T10:47:01.883' AS DateTime), 1, CAST(N'2023-01-20T10:47:17.687' AS DateTime), 1, CAST(N'2023-01-20T14:20:23.527' AS DateTime), 0)
INSERT [dbo].[m_blood_group] ([id], [code], [description], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (6, N'Hijau', N'Mahkluk Asing / Alien', 1, CAST(N'2023-01-20T10:47:57.377' AS DateTime), 1, CAST(N'2023-01-20T15:54:04.097' AS DateTime), 1, CAST(N'2023-01-25T16:10:27.583' AS DateTime), 1)
INSERT [dbo].[m_blood_group] ([id], [code], [description], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (7, N'ungu', N'badut', 1, CAST(N'2023-01-20T14:39:21.693' AS DateTime), NULL, NULL, 1, CAST(N'2023-01-20T14:39:34.597' AS DateTime), 1)
INSERT [dbo].[m_blood_group] ([id], [code], [description], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (8, N'test', N'+', 1, CAST(N'2023-01-20T15:58:37.823' AS DateTime), 1, CAST(N'2023-01-20T16:00:37.197' AS DateTime), 1, CAST(N'2023-02-02T10:51:55.100' AS DateTime), 1)
INSERT [dbo].[m_blood_group] ([id], [code], [description], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (9, N'S', N'+', 1, CAST(N'2023-02-02T10:52:44.620' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_blood_group] OFF
GO
SET IDENTITY_INSERT [dbo].[m_courier] ON 

INSERT [dbo].[m_courier] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (1, N'Shopee Express', 1, CAST(N'2023-04-19T10:14:57.020' AS DateTime), 1, CAST(N'2023-04-19T10:14:57.020' AS DateTime), 1, CAST(N'2023-04-19T10:14:57.020' AS DateTime), 0)
INSERT [dbo].[m_courier] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (13, N'Tokped', 1, CAST(N'2023-04-19T11:03:04.500' AS DateTime), 1, CAST(N'2023-04-19T11:03:04.500' AS DateTime), 1, CAST(N'2023-04-19T11:03:04.500' AS DateTime), 0)
INSERT [dbo].[m_courier] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (14, N'Tiki IDE', 1, CAST(N'2023-04-19T11:04:48.640' AS DateTime), 1, CAST(N'2023-04-19T11:04:48.640' AS DateTime), 1, CAST(N'2023-04-19T11:04:48.640' AS DateTime), 0)
INSERT [dbo].[m_courier] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (22, N'Gojek', 1, CAST(N'2023-04-25T08:35:38.510' AS DateTime), 1, CAST(N'2023-04-25T08:35:38.510' AS DateTime), 1, CAST(N'2023-04-25T08:35:38.510' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[m_courier] OFF
GO
SET IDENTITY_INSERT [dbo].[m_courier_type] ON 

INSERT [dbo].[m_courier_type] ([id], [courier_id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (1, 1, N'ShameDay 7H', 1, CAST(N'2023-04-20T09:30:38.907' AS DateTime), 1, CAST(N'2023-04-20T09:30:38.907' AS DateTime), 1, CAST(N'2023-04-20T09:30:38.907' AS DateTime), 1)
INSERT [dbo].[m_courier_type] ([id], [courier_id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (2, 2, N'JNT Cargo Big Size', 1, CAST(N'2023-04-20T09:10:45.013' AS DateTime), 1, CAST(N'2023-04-20T09:10:45.013' AS DateTime), 1, CAST(N'2023-04-20T09:10:45.013' AS DateTime), 0)
INSERT [dbo].[m_courier_type] ([id], [courier_id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (3, 3, N'Tiki Express', 1, CAST(N'2023-04-20T09:30:41.387' AS DateTime), 1, CAST(N'2023-04-20T09:30:41.387' AS DateTime), 1, CAST(N'2023-04-20T09:30:41.387' AS DateTime), 1)
INSERT [dbo].[m_courier_type] ([id], [courier_id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (4, 4, N'Tiki IDE', 1, CAST(N'2023-04-20T09:12:16.530' AS DateTime), 1, CAST(N'2023-04-20T09:12:16.530' AS DateTime), 1, CAST(N'2023-04-20T09:12:16.530' AS DateTime), 0)
INSERT [dbo].[m_courier_type] ([id], [courier_id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (5, 3, N'JNT Cargo Big Size', 1, CAST(N'2023-04-20T09:30:35.747' AS DateTime), 1, CAST(N'2023-04-20T09:30:35.747' AS DateTime), 1, CAST(N'2023-04-20T09:30:35.747' AS DateTime), 1)
INSERT [dbo].[m_courier_type] ([id], [courier_id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (6, 3, N'JNT Cargo Big Size', 1, CAST(N'2023-04-20T09:33:24.267' AS DateTime), 1, CAST(N'2023-04-20T09:33:24.267' AS DateTime), 1, CAST(N'2023-04-20T09:33:24.267' AS DateTime), 0)
INSERT [dbo].[m_courier_type] ([id], [courier_id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (7, 1, N'Shopee ShameDay 7H', 1, CAST(N'2023-04-20T09:36:36.253' AS DateTime), 1, CAST(N'2023-04-20T09:36:36.253' AS DateTime), 1, CAST(N'2023-04-20T09:36:36.253' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[m_courier_type] OFF
GO
SET IDENTITY_INSERT [dbo].[m_doctor] ON 

INSERT [dbo].[m_doctor] ([id], [biodata_id], [str], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (1, 1, N'DR001', 1, CAST(N'2023-04-27T09:51:36.980' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_doctor] ([id], [biodata_id], [str], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (2, 2, N'DR002', 1, CAST(N'2023-04-27T09:51:54.910' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_doctor] ([id], [biodata_id], [str], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (3, 3, N'DR003', 1, CAST(N'2023-04-27T09:52:10.253' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_doctor] OFF
GO
SET IDENTITY_INSERT [dbo].[m_doctor_education] ON 

INSERT [dbo].[m_doctor_education] ([id], [doctor_id], [education_level_id], [institution_name], [major], [start_year], [end_year], [is_last_education], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (1, 1, 1, N'Universitas Indonesia', N'Kedokteran Gigi', N'1990', N'1994', 0, 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_doctor_education] ([id], [doctor_id], [education_level_id], [institution_name], [major], [start_year], [end_year], [is_last_education], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (2, 1, 2, N'Universitas Indonesia', N'Kedokteran Gigi', N'1994', N'1997', 1, 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_doctor_education] ([id], [doctor_id], [education_level_id], [institution_name], [major], [start_year], [end_year], [is_last_education], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (3, 2, 1, N'Universitan Padjadjaran', N'Kedoctoran Umum', N'2011', N'2016', 0, 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_doctor_education] ([id], [doctor_id], [education_level_id], [institution_name], [major], [start_year], [end_year], [is_last_education], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (4, 2, 2, N'Universitan Padjadjaran', N'Specialis Anak', N'2016', N'2019', 1, 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_doctor_education] OFF
GO
SET IDENTITY_INSERT [dbo].[m_education_level] ON 

INSERT [dbo].[m_education_level] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (1, N'S1', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_education_level] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (2, N'S2', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_education_level] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (3, N'S3', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_education_level] OFF
GO
SET IDENTITY_INSERT [dbo].[m_location] ON 

INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (1, N'DKI Jakarta', NULL, 1, 1, CAST(N'2023-01-23T13:36:54.773' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (2, N'Jakarta Selatan', 1, 2, 1, CAST(N'2023-01-23T13:37:27.223' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (3, N'Jakarta Timur', 1, 2, 1, CAST(N'2023-01-23T14:05:24.343' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (4, N'Ciracas', 3, 4, 1, CAST(N'2023-01-23T14:43:42.583' AS DateTime), NULL, NULL, 1, CAST(N'2023-01-24T14:52:30.770' AS DateTime), 0)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (5, N'Ciracas', 4, 5, 1, CAST(N'2023-01-23T16:14:59.490' AS DateTime), NULL, NULL, 1, CAST(N'2023-01-24T16:26:55.333' AS DateTime), 1)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (6, N'Cipayung', 4, 5, 1, CAST(N'2023-01-24T11:45:41.627' AS DateTime), NULL, NULL, 1, CAST(N'2023-01-25T16:21:50.320' AS DateTime), 1)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (7, N'Ciracas', 4, 5, 1, CAST(N'2023-01-24T11:46:03.153' AS DateTime), NULL, NULL, 1, CAST(N'2023-01-24T11:46:18.983' AS DateTime), 1)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (8, N'Ciracas', 3, 4, 1, CAST(N'2023-01-24T12:00:09.797' AS DateTime), NULL, NULL, 1, CAST(N'2023-01-24T12:00:18.863' AS DateTime), 1)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (9, N'Jawa Barat', NULL, 1, 1, CAST(N'2023-01-25T09:15:47.793' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (10, N'Depok', 9, 2, 1, CAST(N'2023-01-25T16:20:49.080' AS DateTime), NULL, NULL, 1, CAST(N'2023-02-02T15:34:39.630' AS DateTime), 1)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (11, N'Depok', 1, 2, 1, CAST(N'2023-01-25T16:29:51.053' AS DateTime), NULL, NULL, 1, CAST(N'2023-01-26T09:18:11.683' AS DateTime), 1)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (12, N'Depok', 1, 2, 1, CAST(N'2023-01-25T16:30:05.773' AS DateTime), NULL, NULL, 1, CAST(N'2023-01-25T16:30:12.990' AS DateTime), 1)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (13, N'Ciracas', 2, 4, 1, CAST(N'2023-02-02T15:35:16.470' AS DateTime), NULL, NULL, 1, CAST(N'2023-02-02T15:52:16.113' AS DateTime), 1)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (14, N'Ciracas', 3, 4, 1, CAST(N'2023-02-02T15:35:43.143' AS DateTime), NULL, NULL, 1, CAST(N'2023-02-02T15:35:51.467' AS DateTime), 1)
INSERT [dbo].[m_location] ([id], [name], [parent_id], [location_level_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (15, N'Ciracas', 2, 4, 1, CAST(N'2023-02-02T15:48:14.570' AS DateTime), NULL, NULL, 1, CAST(N'2023-02-02T15:48:21.383' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[m_location] OFF
GO
SET IDENTITY_INSERT [dbo].[m_location_level] ON 

INSERT [dbo].[m_location_level] ([id], [name], [abbreviation], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (1, N'Provinsi', N'Prov.', 1, CAST(N'2023-01-23T13:36:15.533' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_location_level] ([id], [name], [abbreviation], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (2, N'Kota', N'Kota.', 1, CAST(N'2023-01-23T13:36:15.533' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_location_level] ([id], [name], [abbreviation], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (3, N'Kabupaten', N'Kab.', 1, CAST(N'2023-01-23T13:36:15.533' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_location_level] ([id], [name], [abbreviation], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (4, N'Kecamatan', N'Kec.', 1, CAST(N'2023-01-23T13:36:15.533' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_location_level] ([id], [name], [abbreviation], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (5, N'Kelurahan', N'Kel.', 1, CAST(N'2023-01-23T13:36:15.533' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_location_level] ([id], [name], [abbreviation], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (6, N'Desa', N'Desa.', 1, CAST(N'2023-01-23T13:36:15.533' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_location_level] OFF
GO
SET IDENTITY_INSERT [dbo].[m_medical_facility] ON 

INSERT [dbo].[m_medical_facility] ([id], [name], [medical_facility_category_id], [location_id], [full_address], [email], [phone_code], [phone], [fax], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (1, N'RS Cipto Mangunkusumo', 1, 2, N'05097 Anthes Terrace', N'abramer0@sohu.com', N'718', N'718-430-9101', N'6326', 1, CAST(N'2023-01-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility] ([id], [name], [medical_facility_category_id], [location_id], [full_address], [email], [phone_code], [phone], [fax], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (2, N'RS Hermina', 1, 4, N'41446 Grim Lane', N'dsyms1@chronoengine.com', N'543', N'543-524-3839', N'2914', 1, CAST(N'2023-01-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility] ([id], [name], [medical_facility_category_id], [location_id], [full_address], [email], [phone_code], [phone], [fax], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (3, N'KL Ciputra Medical Center', 2, 3, N'093874 Turona Madikase', N'hornase@yahio.com', N'900', N'900-212-8763', N'89', 1, CAST(N'2023-01-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility] ([id], [name], [medical_facility_category_id], [location_id], [full_address], [email], [phone_code], [phone], [fax], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (4, N'KL Dewa Medika', 2, 4, N'41554 Ojak Lane', N'illusi@sdarsd.com', N'789', N'789-524-3839', N'9876', 1, CAST(N'2023-01-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility] ([id], [name], [medical_facility_category_id], [location_id], [full_address], [email], [phone_code], [phone], [fax], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (5, N'RS Mitra', 1, 1, N'Jl Kebenaaran Kav 3S', N'illusi@sdarsd.com', N'789', N'789-524-3839', N'9876', 1, CAST(N'2023-01-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility] ([id], [name], [medical_facility_category_id], [location_id], [full_address], [email], [phone_code], [phone], [fax], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (6, N'RSIA Bunda', 1, 1, N'Jl Kesabaran no 932', N'illusi@sdarsd.com', N'789', N'789-524-3839', N'9876', 1, CAST(N'2023-01-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_medical_facility] OFF
GO
SET IDENTITY_INSERT [dbo].[m_medical_facility_category] ON 

INSERT [dbo].[m_medical_facility_category] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (1, N'Rumah Sakit', 1, CAST(N'2023-01-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_category] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (2, N'Klinik', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_category] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (3, N'Puskesmas', 1, CAST(N'2023-01-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_medical_facility_category] OFF
GO
SET IDENTITY_INSERT [dbo].[m_medical_facility_schedule] ON 

INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (1, 1, N'Senin', N'09.00', N'12.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (2, 1, N'Senin', N'12.00', N'15.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (3, 1, N'Senin', N'15.00', N'18.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (4, 1, N'Senin', N'18.00', N'22.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (5, 1, N'Selasa', N'07.00', N'15.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (6, 1, N'Selasa', N'16.00', N'24.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (7, 1, N'Rabu', N'08.00', N'15.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (8, 1, N'Rabu', N'15.00', N'21.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (9, 2, N'Senin', N'07.00', N'13.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (10, 2, N'Senin', N'13.00', N'19.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (11, 2, N'Selasa', N'07.00', N'13.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (12, 2, N'Selasa', N'13.00', N'19.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (13, 2, N'Rabu', N'07.00', N'13.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (14, 2, N'Rabu', N'13.00', N'19.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (15, 5, N'Senin', N'18.00', N'21.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (16, 5, N'Selasa', N'09.00', N'12.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (17, 6, N'Rabu', N'09.00', N'12.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_facility_schedule] ([id], [medical_facility_id], [day], [time_schedule_start], [time_schedule_end], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (18, 5, N'Kamis', N'18.00', N'21.00', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_medical_facility_schedule] OFF
GO
SET IDENTITY_INSERT [dbo].[m_medical_item] ON 

INSERT [dbo].[m_medical_item] ([id], [name], [medical_item_category_id], [composition], [medical_item_segmentation_id], [manufacturer], [indication], [dosage], [directions], [contraindication], [caution], [packaging], [price_max], [price_min], [image], [image_path], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (1, N'Neozep Forte', 5, N'Phenylpropanolamine HCl 15 mg, Paracetamol 250 mg, Salicylamide 150 mg, Chlorpheniramine maleate 2 mg', 2, N'Darya Varia Laboratoria', N'Mengobati gejala flu, seperti: hidung tersumbat, demam, pusing, dan bersin-bersin', N'Dewasa: 3-4 x sehari 1 tablet Anak usia 6-12 tahun: 3-4 x sehari setengah tablet.', N'Obat diminum sesudah makan', N'hipersensitif', N'Peringatan : Awas, Obat Keras Bacalah Aturan Pakainya. Tidak boleh diberikan pada penderita yang sensitif atau alergi terhadap obat simpatomimetik lain (seperti: efedrin, pseudoefedrin, fenilefrin), penderita tekanan darah tinggi, dan individu yang sedang mengkonsumsi anti depresan tipe penghambat monoamin oksidase (MAOI). Hati-hati penggunaan obat ini pada individu yang berpotensi mengalami tekanan darah tinggi atau stroke, atau individu usia lanjut. Hentikan penggunaan obat ini jika terjadi susah tidur, jantung berdebar, dan pusing. Kategori kehamilan : Kategori C: Mungkin berisiko. Obat digunakan dengan hati-hati apabila besarnya manfaat yang diperoleh melebihi besarnya risiko terhadap janin. Penelitian pada hewan uji menunjukkan risiko terhadap janin dan belum terdapat penelitian langsung terhadap wanita hamil.', N'Per Strip @ 4 Tablet', 3100, 2100, NULL, N'neozep.png', 1, CAST(N'2023-01-26T16:11:25.140' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_item] ([id], [name], [medical_item_category_id], [composition], [medical_item_segmentation_id], [manufacturer], [indication], [dosage], [directions], [contraindication], [caution], [packaging], [price_max], [price_min], [image], [image_path], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (2, N'OBH Combi', 6, N'Tiap 5 ml mengandung : Succus Liquiritiae 167 mg, Paracetamol 150 mg, Ammonium Chloride 50 mg, Ephedrine HCl 2.5 mg, Chlorpheniramine maleate 1 mg', 2, N'Combiphar', N'Untuk meredakan batuk yang disertai gejala-gejala flu seperti demam, sakit kepala, hidung tersumbat dan bersin-bersin.', N'Dewasa dan anak-anak di atas 12 tahun: 3 kali sehari 15 mL', N'Berikan sesudah makan', N'Penderita dengan gangguan jantung, diabetes mellitus, gangguan fungsi hati yang berat dan hipersensitif terhadap komponen obat.', N'Dapat menyebabkan kantuk. Hati-hati penggunaannya pada penderita dengan gangguan fungsi hati dan ginjal, galukoma, hipertrofi prostat, hipertiroid dan retensi urin. Tidak dianjurkan pada anak usia di bawah 6 tahun, wanita hamil dan menyusui, kecuali atas petunjuk dokter. Hati-hati penggunan bersama dengan obat-obat lain yang meneka susunan saraf pusat. Selama minum obat ini tidak boleh mengendari kendaraan bermotor atau menjalankan mesin. Penggunaan pada penderita yang mengonsumsi alkohol dapat meningkatkan risiko kerusakan fungsi hati.', N'Per Botol @ 60 ml', 15700, 11500, NULL, N'obh combi.jfif', 1, CAST(N'2023-01-26T16:11:25.140' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_item] ([id], [name], [medical_item_category_id], [composition], [medical_item_segmentation_id], [manufacturer], [indication], [dosage], [directions], [contraindication], [caution], [packaging], [price_max], [price_min], [image], [image_path], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (3, N'Panadol Anak-Anak', 1, N'Tiap 5 ml mengandung : Paracetamol 160 mg', 2, N'Sterling', N'Obat ini dapat digunakan untuk meredakan rasa sakit seperti sakit kepala, sakit gigi dan menurunkan demam yang menyertai flu dan demam sesudah imunisasi atau vaksinasi', N'Anak usia 1-2 tahun: 3.75 ml, diberikan 3-4 kali sehari. Anak usia 2-3 tahun: 1 sendok takar (5 ml), diberikan 3-4 kali sehari. Anak usia 4-5 tahun: 1.5 sendok takar (7.5 ml), diberikan 3-4 kali sehari. Anak usia 6 tahun: 2 sendok takar (10 ml), diberikan 3-4 kali sehari. Maksimum interval penggunaan dosis adalah 4 jam dan tidak melebihi 4 kali dalam 24 jam. Jangan melebihi dosis yang dianjurkan atau sesuai anjuran dokter.', N'Dikonsumsi sebelum atau sesudah makan. Dapat langsung diminum atau dicampur dengan air/ sari buah.', N'Penderita yang hipersensitif terhadap paracetamol dan bahan lain dalam obat ini. Penderita dengan gangguan fungsi hati.', N'Hati-hati penggunaan obat ini pada penderita penyakit ginjal. Bila setelah 2 hari demam tidak menurun atau setelah 5 hari nyeri tidak menghilang, segera hubungi Unit Pelayanan Kesehatan. Penggunaan obat ini pada penderita yang mengkonsumsi alkohol, dapat meningkatkan risiko kerusakan fungsi hati. Kategori kehamilan : Kategori B: Mungkin dapat digunakan oleh wanita hamil. Penelitian pada hewan uji tidak memperlihatkan ada nya risiko terhadap janin, namun belum ada bukti penelitian langsung terhadap wanita hamil.', N'Per Botol @ 30 ml', 32100, 22800, NULL, N'panadol anak.jpeg', 1, CAST(N'2023-01-27T11:37:34.630' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_item] ([id], [name], [medical_item_category_id], [composition], [medical_item_segmentation_id], [manufacturer], [indication], [dosage], [directions], [contraindication], [caution], [packaging], [price_max], [price_min], [image], [image_path], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (4, N'Panadol Extra', 4, N'Tiap kaplet mengandung Paracetamol 500 mg dan Caffeine 65 mg.', 2, N'Sterling', N'Obat ini digunakan untuk meringankan sakit kepala dan sakit gigi.', N'Dewasa dan anak-anak lebih dari 12 tahun : 1 Kaplet, ditelan dengan segelas air, 3-4 kali sehari bila gejala memburuk. Tidak melebihi 8 kaplet dalam 24 jam. Minimum interval penggunaan dosis adalah 4 jam.', N'Sebelum atau sesudah makan.', N'Wanita hamil dan menyusui. Tidak dianjurkan untuk digunakan pada anak dibawah usia 12 tahun', N'Hati-hati penggunaan obat ini pada penderita penyakit ginjal. Bila setelah 2 hari demam tidak menurun atau setelah 5 hari nyeri tidak menghilang, segera hubungi Unit Pelayanan Kesehatan. Penggunaan obat ini pada penderita yang mengkonsumsi alkohol, dapat meningkatkan risiko kerusakan fungsi hati. Kategori kehamilan : Kategori C: Mungkin berisiko. Obat digunakan dengan hati-hati apabila besarnya manfaat yang diperoleh melebihi besarnya risiko terhadap janin. Penelitian pada hewan uji menunjukkan risiko terhadap janin dan belum terdapat penelitian langsung terhadap wanita hamil.', N'Per Strip @ 10 Kaplet', 15700, 9200, NULL, N'panadol extra.png', 1, CAST(N'2023-01-27T11:37:34.630' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_item] ([id], [name], [medical_item_category_id], [composition], [medical_item_segmentation_id], [manufacturer], [indication], [dosage], [directions], [contraindication], [caution], [packaging], [price_max], [price_min], [image], [image_path], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (5, N'Panadol Flu & Batuk', 6, N'Setiap tablet mengandung:Paracetamol 500 mg, Phenylephrine HCL 5 mg, dan Dextromethorphan HBr 15 mg.', 2, N'Sterling', N'Obat ini digunakan untuk meredakan gejala flu seperti demam, sakit kepala, hidung tersumbat dan batuk tidak berdahak.', N'Dewasa: 1 kaplet 3 kali sehari Jangan melebihi 8 kaplet dalam tiap jangka waktu 24 jam. Jangan diberikan kepada anak-anak di bawah umur 12 tahun, kecuali atas petunjuk dokter.', N'Dikonsumsi sesudah makan', N'Penderita hipersensitif terhadap komponen obat ini. Penderita dengan gangguan fungsi hati yang berat, gangguan jantung, dan diabetes mellitus.', N'Awas! Obat keras, bacalah aturan memakainya. Hati- hati penggunaan pada penderita tekanan darah tinggi atau mempunyai potensi tekanan darah tinggi atau stroke, seperti pada penderita dengan berat badan berlebih atau penderita usia lanjut. Bila dalam 3 hari gejala flu tidak berkurang segera hubungi dokter. Hentikan penggunaan obat jika terjadi susah tidur, jantung berdebar, dan pusing. Simpan dalam wadah tertutup rapat, ditempat kering dan dibawah 30 C', N'Per Strip @ 10 Kaplet', 16600, 11700, NULL, N'panadol flu dan batuk.jpg', 1, CAST(N'2023-01-27T11:37:34.630' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_item] ([id], [name], [medical_item_category_id], [composition], [medical_item_segmentation_id], [manufacturer], [indication], [dosage], [directions], [contraindication], [caution], [packaging], [price_max], [price_min], [image], [image_path], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (6, N'Rhinos', 6, N'Tiap kapsul mengandung: Loratadine 5 mg Pseudoephedrine HCI 120 mg', 1, N'Dexa Medica', N'INFORMASI OBAT INI HANYA UNTUK KALANGAN MEDIS. Meredakan gejala yang berhubungan dengan rinitis alergi misalnya bersin, hidung tersumbat, rinore, pruritus & lakrimasi.', N'PENGGUNAAN OBAT INI HARUS SESUAI DENGAN PETUNJUK DOKTER. Dewasa dan anak diatas 12 tahun: 1 kapsul, 2 kali perhari atau setiap 12 jam.', N'Diminum sebelum atau sesudah makan', N'Hipersensitivitas terhadap agen adrenergik. Penyakit KV misalnya, insufisiensi koroner, aritmia; pasien yang menerima terapi MAOI atau dalam 10 hari setelah penghentian pengobatan tersebut. Glaukoma sudut sempit, retensi urin, hipertensi berat, CAD berat, hipertiroidisme.', N'HARUS DENGAN RESEP DOKTER. Hipertensi, DM, insufisiensi hati & ginjal; glaukoma, ulkus peptikum stenosis, obstruksi pyloroduodenal, hipertrofi prostat, obstruksi leher kandung kemih, penyakit KV, peningkatan TIO. Dapat menyebabkan penyalahgunaan obat. Kehamilan & laktasi. Anak <12 thn. Lansia 60 thn. Kategori Kehamilan: Belum terdapat data keamanan terkait penggunaan obat ini pada wanita hamil dan/atau menyusui. Konsultasikan kepada dokter apabila Anda sedang hamil dan/atau menyusui.', N'Per Strip @ 10 Kapsul', 90000, 80000, NULL, N'rhinos.png', 1, CAST(N'2023-01-27T13:37:53.347' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_item] ([id], [name], [medical_item_category_id], [composition], [medical_item_segmentation_id], [manufacturer], [indication], [dosage], [directions], [contraindication], [caution], [packaging], [price_max], [price_min], [image], [image_path], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (7, N'Paracetamol', 1, N'Setiap tablet mengandung Paracetamol 500 mg', 2, N'Generic Manufacturer', N'Obat ini digunakan untuk meredakan nyeri ringan hingga sedang seperti sakit kepala, sakit gigi, nyeri otot, serta menurunkan demam.', N'Dewasa: 1-2 kaplet, 3-4 kali per hari. Penggunaan maximum 8 kaplet per hari. Anak 7-12 tahun : 0.5 - 1 kaplet, 3-4 kali per hari. Penggunaan maximum 4 kaplet per hari.', N'Obat dapat diminum sebelum atau sesudah makan', N'Parasetamol jangan diberikan kepada penderita hipersensitif/alergi terhadap Paracetamol. Penderita gangguan fungsi hati berat.', N'Hati-hati penggunaan pada pasien dengan gagal ginjal, gangguan fungsi hati, dan alergi atau mengalami hipersensitivitas terhadap paracetamol. Kategori kehamilan : Kategori B: Mungkin dapat digunakan oleh wanita hamil. Penelitian pada hewan uji tidak memperlihatkan ada nya risiko terhadap janin, namun belum ada bukti penelitian langsung terhadap wanita hamil.', N'Per Strip @ 10 Tablet', 5200, 1500, NULL, N'Paracetamol.jpeg', 1, CAST(N'2023-01-27T13:37:53.347' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_item] ([id], [name], [medical_item_category_id], [composition], [medical_item_segmentation_id], [manufacturer], [indication], [dosage], [directions], [contraindication], [caution], [packaging], [price_max], [price_min], [image], [image_path], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (8, N'Cataflam', 4, N'Kalium Diklofenak 50 mg', 1, N'Novartis Indonesia', N'INFORMASI OBAT INI HANYA UNTUK KALANGAN MEDIS. Terapi akut dan kronik gejala RA (Rheumatoid Arthritis), OA (Osteoarthritis) & Spondilitis Ankilosa, mengatasi nyeri & Dismenorea primer, bila diinginkan efek segera', N'PENGGUNAAN OBAT INI HARUS SESUAI DENGAN PETUNJUK DOKTER. Dewasa dan Anak diatas 14 tahun: 25 mg atau 50 mg, 2 sampai 3 kali per hari.', N'Sesudah makan', N'Hipersensitivitas. Ulserasi GI aktif, perdarahan atau perforasi. Pasien yang pernah mengalami asma, urtikaria atau reaksi alergi setelah terapi aspirin atau NSAID. Nyeri peri-op pada operasi CABG. CHF [New York Heart Association (NYHA) kelas II-IV]. Penyakit jantung iskemik. Penyakit arteri perifer &/atau serebrovaskular. Ginjal (GFR <15 mL/mnt/1,73 m2) & gagal hati. Hamil (trimester terakhir) & ibu menyusui.', N'HARUS DENGAN RESEP DOKTER. Hati-hati penggunaan pada pasien yang diketahui atau berisiko mengalami penyakit hipertensi, retensi cairan atau gagal jantung, riwayat tukak atau perdarahan GI. Ketepatan diagnosis & pengamatan medis secara ketat perlu dilakukan pd pasien dg gejala/riwayat ggn GI, penyakit Crohn, ggn fungsi hati, ginjal, & jantung, asma, rinitis alergi musiman, pembengkakan mukosa hidung, PPOK atau infeksi sal napas kronik. Pantau tes fungsi hati, Tekanan darah (pd penggunaan lama). Pasien dg porfiria hepatik. Lanjut usia, anak. Dpt mengganggu kemampuan mengemudi atau menjalankan mesin. Kategori kehamilan : Kategori C: Mungkin berisiko. Obat digunakan dengan hati-hati apabila besarnya manfaat yang diperoleh melebihi besarnya risiko terhadap janin. Penelitian pada hewan uji menunjukkan risiko terhadap janin dan belum terdapat penelitian langsung terhadap wanita hamil. Pada trimester ketiga Kategori D: Terbukti berisiko terhadap janin. Meski demikian, obat masih dapat digunakan jika obat diperlukan untuk mengatasi keadaan yang mengancam jiwa, atau penyakit serius, dimana obat yang lebih aman tidak dapat digunakan atau tidak efektif.', N'Per Strip @ 10 Tablet', 83100, 73000, NULL, N'cataflam.jpeg', 1, CAST(N'2023-01-27T13:37:53.347' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_medical_item] OFF
GO
SET IDENTITY_INSERT [dbo].[m_medical_item_category] ON 

INSERT [dbo].[m_medical_item_category] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (1, N'Demam', 1, CAST(N'2023-01-26T13:34:44.047' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_item_category] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (2, N'Sakit Kepala', 1, CAST(N'2023-01-26T13:34:44.047' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_item_category] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (3, N'Sakit Gigi', 1, CAST(N'2023-01-26T13:34:44.047' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_item_category] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (4, N'Nyeri', 1, CAST(N'2023-01-26T13:34:44.047' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_item_category] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (5, N'Flu', 1, CAST(N'2023-01-26T13:34:44.047' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_item_category] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (6, N'Batuk', 1, CAST(N'2023-01-26T13:34:44.047' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_medical_item_category] OFF
GO
SET IDENTITY_INSERT [dbo].[m_medical_item_segmentation] ON 

INSERT [dbo].[m_medical_item_segmentation] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (1, N'Obat keras', 1, CAST(N'2023-01-26T13:37:20.117' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_medical_item_segmentation] ([id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [delted_on], [is_delete]) VALUES (2, N'Obat Bebas', 1, CAST(N'2023-01-26T13:37:20.117' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_medical_item_segmentation] OFF
GO
SET IDENTITY_INSERT [dbo].[m_menu] ON 

INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (1, N'Master', N'', 0, N'nav-icon fas fa-light', N'nav-icon fas fa-light', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (2, N'Transaction', N'', 0, N'nav-icon fas fa-light', N'nav-icon fas fa-light', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (3, N'Home', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (4, N'Admin', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (5, N'Bank', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (6, N'Biodata', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (7, N'BiodataAddress', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (8, N'BloodGroup', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (9, N'Courier', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (10, N'CourierType', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (11, N'CourierDiscount', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (12, N'Customer', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (13, N'CustomerMember', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (14, N'CustomerRelation', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (15, N'CustomerChat', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (16, N'CustomerChatHistory', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (17, N'CustCustNominal', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (18, N'CustomerRegisCard', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (19, N'CustomerVA', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (20, N'CustomerVAHist', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (21, N'CustomerWallet', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (22, N'CustomerWallTP', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (23, N'CustomerWallWD', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (24, N'Doctor', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (25, N'DoctorEducation', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (26, N'DoctorTreatment', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (27, N'DoctorOffice', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (28, N'DoctorOfficeSche', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (29, N'DoctorOfficeTreat', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (30, N'DoctorOfcTreatPrice', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (31, N'CurrentDoctorSpez', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (32, N'EducationLevel', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (33, N'Location', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (34, N'LocationLevel', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (35, N'MedicalFacility', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (36, N'MedFacilityCatg', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (37, N'MedFacilitySch', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (38, N'MedicalItem', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (39, N'MedItemCatg', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (40, N'MedItemSegmen', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (41, N'MedlItemPurch', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (42, N'MedItemPurchDet', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (43, N'Menu', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (44, N'MenuRole', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (45, N'PaymentMethod', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (46, N'Role', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (47, N'Spezialization', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (48, N'User', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (49, N'Wallet', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (50, N'BioAttachment', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (51, N'Appointment', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (52, N'AppointmentResch', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu] ([Id], [name], [url], [parent_id], [big_icon], [small_icon], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (53, N'TreatmentDisc', N'Index', 1, N'far fa-light fa-file nav-icon', N'far fa-light fa-file nav-icon', 1, CAST(N'2023-04-26T11:26:58.893' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_menu] OFF
GO
SET IDENTITY_INSERT [dbo].[m_menu_role] ON 

INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (1, 1, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (2, 2, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (3, 3, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (4, 4, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (5, 5, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (6, 6, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (7, 7, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (8, 8, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (9, 9, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (10, 10, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (11, 11, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (12, 12, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (13, 13, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (14, 14, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (15, 15, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (16, 16, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (17, 17, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (18, 18, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (19, 19, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (20, 20, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (21, 21, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (22, 22, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (23, 23, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (24, 24, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (25, 25, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (26, 26, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (27, 27, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (28, 28, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (29, 29, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (30, 30, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (31, 31, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (32, 32, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (33, 33, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (34, 34, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (35, 35, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (36, 36, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (37, 37, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (38, 38, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (39, 39, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (40, 40, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (41, 41, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (42, 42, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (43, 43, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (44, 44, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (45, 45, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (46, 46, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (47, 47, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (48, 48, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (49, 49, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (50, 50, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (51, 51, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (52, 52, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (53, 53, 1, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (54, 1, 2, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (55, 3, 2, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (56, 6, 2, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (57, 24, 2, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (58, 31, 2, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (59, 47, 2, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (60, 6, 3, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (61, 3, 3, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (62, 9, 3, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (63, 10, 3, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (64, 1, 3, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (65, 1, 4, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (66, 3, 4, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (67, 5, 4, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (68, 6, 4, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (69, 7, 4, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (70, 12, 4, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (71, 24, 4, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (72, 37, 4, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (73, 38, 4, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (74, 50, 4, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_menu_role] ([Id], [menu_Id], [role_Id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (75, 51, 4, 1, CAST(N'2023-04-26T13:59:10.083' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_menu_role] OFF
GO
SET IDENTITY_INSERT [dbo].[m_role] ON 

INSERT [dbo].[m_role] ([Id], [name], [code], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (1, N'Admin', N'Admin01', 1, CAST(N'2023-04-20T15:52:44.197' AS DateTime), 1, CAST(N'2023-04-20T15:52:44.197' AS DateTime), 1, CAST(N'2023-04-20T15:52:44.197' AS DateTime), 0)
INSERT [dbo].[m_role] ([Id], [name], [code], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (2, N'Dokter', N'Dokter01', 1, CAST(N'2023-04-20T15:52:44.197' AS DateTime), 1, CAST(N'2023-04-20T15:52:44.197' AS DateTime), 1, CAST(N'2023-04-20T15:52:44.197' AS DateTime), 0)
INSERT [dbo].[m_role] ([Id], [name], [code], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (3, N'Faskes', N'Faskes01', 1, CAST(N'2023-04-20T15:52:44.197' AS DateTime), 1, CAST(N'2023-04-20T15:52:44.197' AS DateTime), 1, CAST(N'2023-04-20T15:52:44.197' AS DateTime), 0)
INSERT [dbo].[m_role] ([Id], [name], [code], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (4, N'Pasien', N'Pasien01', 1, CAST(N'2023-04-20T15:52:44.197' AS DateTime), 1, CAST(N'2023-04-20T15:52:44.197' AS DateTime), 1, CAST(N'2023-04-20T15:52:44.197' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[m_role] OFF
GO
SET IDENTITY_INSERT [dbo].[m_specialization] ON 

INSERT [dbo].[m_specialization] ([Id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (1, N'Dokter Gigi', 1, CAST(N'2023-01-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_specialization] ([Id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (2, N'Dokter Kulit', 1, CAST(N'2023-01-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_specialization] ([Id], [name], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (3, N'Dokter Anak', 1, CAST(N'2023-01-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_specialization] OFF
GO
SET IDENTITY_INSERT [dbo].[m_user] ON 

INSERT [dbo].[m_user] ([Id], [biodata_id], [role_id], [email], [password], [login_attempt], [is_locked], [last_login], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (1, 1, 1, N'admin@gmail.com', N'1234', 0, 0, CAST(N'2023-04-26T11:59:14.753' AS DateTime), 1, CAST(N'2023-04-26T11:59:14.753' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_user] ([Id], [biodata_id], [role_id], [email], [password], [login_attempt], [is_locked], [last_login], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (2, 2, 2, N'dokter@gmail.com', N'1234', 0, 0, CAST(N'2023-04-26T11:59:14.753' AS DateTime), 1, CAST(N'2023-04-26T11:59:14.753' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_user] ([Id], [biodata_id], [role_id], [email], [password], [login_attempt], [is_locked], [last_login], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (3, 3, 3, N'faskes@gmail.com', N'1234', 0, 0, CAST(N'2023-04-26T11:59:14.753' AS DateTime), 1, CAST(N'2023-04-26T11:59:14.753' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_user] ([Id], [biodata_id], [role_id], [email], [password], [login_attempt], [is_locked], [last_login], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (4, 4, 4, N'pasien@gmail.com', N'1234', 0, 0, CAST(N'2023-04-26T11:59:14.753' AS DateTime), 1, CAST(N'2023-04-26T11:59:14.753' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[m_user] ([Id], [biodata_id], [role_id], [email], [password], [login_attempt], [is_locked], [last_login], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (5, 5, 5, N'umum@gmail.com', N'1234', 0, 0, CAST(N'2023-04-26T11:59:14.753' AS DateTime), 1, CAST(N'2023-04-26T11:59:14.753' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[m_user] OFF
GO
SET IDENTITY_INSERT [dbo].[t_current_doctor_specialization] ON 

INSERT [dbo].[t_current_doctor_specialization] ([id], [doctor_id], [specialization_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (1, 1, 0, 1, CAST(N'2023-02-02T08:26:39.793' AS DateTime), 1, CAST(N'2023-02-02T15:34:07.647' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[t_current_doctor_specialization] ([id], [doctor_id], [specialization_id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete]) VALUES (2, 1, 3, 1, CAST(N'2023-02-02T15:33:54.147' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[t_current_doctor_specialization] OFF
GO
SET IDENTITY_INSERT [dbo].[t_doctor_office] ON 

INSERT [dbo].[t_doctor_office] ([id], [doctor_id], [medical_facility_id], [specialization], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (1, 1, 1, N'Dokter Gigi', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office] ([id], [doctor_id], [medical_facility_id], [specialization], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (2, 1, 2, N'Dokter Gigi', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office] ([id], [doctor_id], [medical_facility_id], [specialization], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (3, 2, 5, N'Dokter Anak', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office] ([id], [doctor_id], [medical_facility_id], [specialization], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (4, 2, 6, N'Dokter Anak', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[t_doctor_office] OFF
GO
SET IDENTITY_INSERT [dbo].[t_doctor_office_schedule] ON 

INSERT [dbo].[t_doctor_office_schedule] ([id], [doctor_id], [medical_facility_schedule_id], [slot], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (1, 1, 1, 10, 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office_schedule] ([id], [doctor_id], [medical_facility_schedule_id], [slot], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (2, 1, 7, 10, 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office_schedule] ([id], [doctor_id], [medical_facility_schedule_id], [slot], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (3, 2, 15, 10, 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office_schedule] ([id], [doctor_id], [medical_facility_schedule_id], [slot], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (4, 2, 16, 10, 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office_schedule] ([id], [doctor_id], [medical_facility_schedule_id], [slot], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (5, 2, 17, 10, 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office_schedule] ([id], [doctor_id], [medical_facility_schedule_id], [slot], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (6, 2, 18, 10, 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[t_doctor_office_schedule] OFF
GO
SET IDENTITY_INSERT [dbo].[t_doctor_office_treatment] ON 

INSERT [dbo].[t_doctor_office_treatment] ([id], [doctor_treatment_id], [doctor_office_id], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (1, 1, 1, 1, CAST(N'2023-04-27T14:28:29.963' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office_treatment] ([id], [doctor_treatment_id], [doctor_office_id], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (2, 2, 1, 1, CAST(N'2023-04-27T14:28:29.963' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office_treatment] ([id], [doctor_treatment_id], [doctor_office_id], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (3, 3, 1, 1, CAST(N'2023-04-27T14:28:29.963' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office_treatment] ([id], [doctor_treatment_id], [doctor_office_id], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (4, 4, 1, 1, CAST(N'2023-04-27T14:28:29.963' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[t_doctor_office_treatment] OFF
GO
SET IDENTITY_INSERT [dbo].[t_doctor_office_treatment_price] ON 

INSERT [dbo].[t_doctor_office_treatment_price] ([id], [doctor_office_treatment_id], [price], [price_start_from], [price_until_from], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (1, 1, CAST(500000 AS Decimal(18, 0)), CAST(300000 AS Decimal(18, 0)), CAST(1000000 AS Decimal(18, 0)), 1, CAST(N'2023-04-27T14:21:00.753' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office_treatment_price] ([id], [doctor_office_treatment_id], [price], [price_start_from], [price_until_from], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (2, 2, CAST(600000 AS Decimal(18, 0)), CAST(400000 AS Decimal(18, 0)), CAST(1000000 AS Decimal(18, 0)), 1, CAST(N'2023-04-27T14:21:00.753' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office_treatment_price] ([id], [doctor_office_treatment_id], [price], [price_start_from], [price_until_from], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (3, 3, CAST(700000 AS Decimal(18, 0)), CAST(500000 AS Decimal(18, 0)), CAST(1000000 AS Decimal(18, 0)), 1, CAST(N'2023-04-27T14:21:00.753' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_office_treatment_price] ([id], [doctor_office_treatment_id], [price], [price_start_from], [price_until_from], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (4, 4, CAST(800000 AS Decimal(18, 0)), CAST(600000 AS Decimal(18, 0)), CAST(1000000 AS Decimal(18, 0)), 1, CAST(N'2023-04-27T14:21:00.753' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[t_doctor_office_treatment_price] OFF
GO
SET IDENTITY_INSERT [dbo].[t_doctor_treatment] ON 

INSERT [dbo].[t_doctor_treatment] ([id], [doctor_id], [name], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (1, 1, N'Spesialis ortodonti (Sp.Ort)', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_treatment] ([id], [doctor_id], [name], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (2, 1, N'Spesialis periodonsia (Sp.Perio)', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_treatment] ([id], [doctor_id], [name], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (3, 1, N'Spesialis konservasi gigi (Sp.KG)', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_treatment] ([id], [doctor_id], [name], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (4, 2, N'Fisioterapi anak', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_treatment] ([id], [doctor_id], [name], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (5, 2, N'Konsultasi kesehaan anak', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_treatment] ([id], [doctor_id], [name], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (6, 2, N'Skrining tumbuh kembang anak', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_treatment] ([id], [doctor_id], [name], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (7, 2, N'Vaksin anak', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_treatment] ([id], [doctor_id], [name], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (8, 2, N'Konsultasi alergi anak', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_treatment] ([id], [doctor_id], [name], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (9, 2, N'Konsultasi psikologi anak', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[t_doctor_treatment] ([id], [doctor_id], [name], [create_by], [create_on], [modified_by], [modified_on], [delete_by], [delete_on], [is_delete]) VALUES (10, 2, N'Test pendengaran OAE', 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[t_doctor_treatment] OFF
GO
USE [master]
GO
ALTER DATABASE [med.id] SET  READ_WRITE 
GO
