﻿using Med_Id.datamodels;
using Med_Id.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Med_Id.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiBiodataController : ControllerBase
    {
        private readonly medidContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiBiodataController(medidContext _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<MBiodatum> GetAllData()
        {
            List<MBiodatum> data = db.MBiodata.Where(a => a.IsDelete == false).ToList();
            return data;

        }
        [HttpGet("GetDataById/{id}")]
        public MBiodatum GetDataById(int id)
        {
            MBiodatum result = new MBiodatum();
            result = db.MBiodata.Where(a => a.Id == id).FirstOrDefault()!;
            return result;
        }

        [HttpPost("Save")]
        public VMResponse Save(MBiodatum data)
        {
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data); db.SaveChanges();

                respon.Message = "Data succses saved";
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed Saved" + e.Message;
            }
            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(MBiodatum data)
        {
            MBiodatum dta = db.MBiodata.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dta != null)
            {

                dta.Fullname = data.Fullname;
                dta.MobilePhone = data.MobilePhone;
                dta.Image = data.Image;
                dta.ImagePath = data.ImagePath;
                dta.ModifiedBy = IdUser;
                dta.ModifiedOn = DateTime.Now;

                dta.IsDelete = false;

                try
                {
                    db.Update(dta); db.SaveChanges();

                    respon.Message = "Data Succes Update";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Data Failed" + e.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }
            return respon;
        }

        [HttpDelete("Delete/{id}/{DeletedBy}")]
        public VMResponse Delete(int id, int DeletedBy)
        {
            MBiodatum dta = db.MBiodata.Where(a => a.Id == id).FirstOrDefault()!;

            if (dta != null)
            {
                dta.IsDelete = true;

                dta.DeletedBy = IdUser;
                dta.DeletedOn = DateTime.Now;

                try
                {
                    db.Update(dta);
                    db.SaveChanges();

                    respon.Message = $"Data {dta.Fullname} success deleted";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Delete Failed" + e.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }
            return respon;
        }
    }
}
