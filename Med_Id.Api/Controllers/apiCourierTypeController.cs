﻿using Microsoft.AspNetCore.Mvc;
using Med_Id.datamodels;
using Med_Id.viewmodels;

namespace Med_Id.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCourierTypeController : ControllerBase
    {
        private readonly medidContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiCourierTypeController(medidContext _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMCourierType> GetAllData()
        {
            List<VMCourierType> data = (from v in db.MCourierTypes
                                       join c in db.MCouriers on v.CourierId equals c.Id
                                       where v.IsDelete == false
                                       select new VMCourierType
                                       {
                                           Id = v.Id,
                                           Name = v.Name,
                                           CreatedBy = v.CreatedBy,
                                           CreatedOn = v.CreatedOn,
                                           ModifiedBy = v.ModifiedBy,
                                           ModifiedOn = v.ModifiedOn,
                                           DeletedBy = v.DeletedBy,
                                           DeltedOn = v.DeltedOn,
                                           IsDelete = v.IsDelete,

                                           CourierId = v.CourierId,
                                           NameCourier = c.Name,
                                       }).ToList();
            return data;

        }
        [HttpGet("GetDataById/{id}")]
        public VMCourierType GetDataById(int id)
        {
            VMCourierType data = (from v in db.MCourierTypes
                                  join c in db.MCouriers on v.CourierId equals c.Id
                                  where v.IsDelete == false && v.Id == id
                                 select new VMCourierType
                                 {
                                     Id = v.Id,
                                     Name = v.Name,
                                     CreatedBy = v.CreatedBy,
                                     CreatedOn = v.CreatedOn,
                                     ModifiedBy = v.ModifiedBy,
                                     ModifiedOn = v.ModifiedOn,
                                     DeletedBy = v.DeletedBy,
                                     DeltedOn = v.DeltedOn,
                                     IsDelete = v.IsDelete,

                                     CourierId = v.CourierId,
                                     NameCourier = c.Name,
                                 }).FirstOrDefault()!;
            return data;
        }
        [HttpGet("GetDataByCourierId/{id}")]
        public List<VMCourierType> GetDataByCourierId(int id)
        {
            List<VMCourierType> data = (from v in db.MCourierTypes
                                        join c in db.MCouriers on v.CourierId equals c.Id
                                        where v.IsDelete == false && v.CourierId == id
                                       select new VMCourierType
                                       {
                                           Id = v.Id,
                                           Name = v.Name,
                                           CreatedBy = v.CreatedBy,
                                           CreatedOn = v.CreatedOn,
                                           ModifiedBy = v.ModifiedBy,
                                           ModifiedOn = v.ModifiedOn,
                                           DeletedBy = v.DeletedBy,
                                           DeltedOn = v.DeltedOn,
                                           IsDelete = v.IsDelete,

                                           CourierId = v.CourierId,
                                           NameCourier = c.Name,
                                       }).ToList();
            return data;
        }
        [HttpPost("Save")]
        public VMResponse Save(MCourierType data)
        {
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            data.ModifiedBy = IdUser;
            data.ModifiedOn = DateTime.Now;
            data.DeletedBy = IdUser;
            data.DeltedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data); db.SaveChanges();

                respon.Message = "Data succses saved";
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed Saved" + e.Message;
            }
            return respon;
        }
        [HttpPut("Edit")]
        public VMResponse Edit(MCourierType data)
        {
            MCourierType dta = db.MCourierTypes.Where(a => a.Id == data.Id).FirstOrDefault()!;
            
            if (dta != null)
            {
                dta.Name = data.Name;
                dta.CourierId = data.CourierId;
                dta.Name = data.Name;
                dta.CreatedBy = IdUser;
                dta.CreatedOn = DateTime.Now;
                dta.ModifiedBy = IdUser;
                dta.ModifiedOn = DateTime.Now;
                dta.DeletedBy = IdUser;
                dta.DeltedOn = DateTime.Now;
                dta.IsDelete = false;

                try
                {
                    db.Update(dta); db.SaveChanges();

                    respon.Message = "Data Succes Update";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Data Failed" + e.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }
            return respon;
        }

        [HttpDelete("Delete/{id}/{DeletedBy}")]
        public VMResponse Delete(int id, int DeletedBy)
        {
            MCourierType dta = db.MCourierTypes.Where(a => a.Id == id).FirstOrDefault()!;

            if (dta != null)
            {
                dta.IsDelete = true;
                dta.CreatedBy = IdUser;
                dta.CreatedOn = DateTime.Now;
                dta.ModifiedBy = IdUser;
                dta.ModifiedOn = DateTime.Now;
                dta.DeletedBy = IdUser;
                dta.DeltedOn = DateTime.Now;

                try
                {
                    db.Update(dta);
                    db.SaveChanges();

                    respon.Message = $"Data {dta.Name} success deleted";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Delete Failed" + e.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }
            return respon;
        }
    }
}
