﻿using Med_Id.datamodels;
using Med_Id.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;

namespace Med_Id.Api.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class apiAuthController : ControllerBase
	{
		private readonly medidContext db;
		private VMResponse respon = new VMResponse();
		private int IdUser = 1;

		public apiAuthController(medidContext _db)
		{
			this.db = _db;
		}
        
        [HttpGet("MenuAccess/{IdRole}")]
		public List<VMMenu> MenuAccess(int IdRole)
		{
			List<VMMenu> listMenu = new List<VMMenu>();

			listMenu = (from parent in db.MMenus
						join ma in db.MMenuRoles
						on parent.Id equals ma.MenuId
						where ma.RoleId == IdRole && parent.ParentId == 0 && parent.IsDelete == false && ma.IsDelete == false
						select new VMMenu
						{
							Id = parent.Id,
							Name = parent.Name,
							Url = parent.Url,
							ParentId = parent.ParentId,
							BigIcon = parent.BigIcon,
							SmallIcon = parent.SmallIcon,

							RoleId = ma.RoleId,

							ListChild = (from child in db.MMenus
                                         join ma2 in db.MMenuRoles
										 on child.Id equals ma2.MenuId
                                         where child.ParentId == parent.Id && child.IsDelete == false && ma2.IsDelete == false && ma2.RoleId == IdRole
										 select new VMMenu
										 {
											 Id = child.Id,
											 Name = child.Name,
											 Url = child.Url,
											 ParentId = child.ParentId,
											 BigIcon = child.BigIcon,
											 SmallIcon = child.SmallIcon,
                                             RoleId = IdRole

                                         }).OrderBy(a => a.Name).ToList()
						}).OrderBy(a => a.Name).ToList();

			return listMenu;
		}

	}
}