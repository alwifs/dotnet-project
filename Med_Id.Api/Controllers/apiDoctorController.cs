﻿using Med_Id.datamodels;
using Med_Id.viewmodels;
using Microsoft.AspNetCore.Mvc;

namespace Med_Id.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiDoctorController : Controller
    {
        private readonly medidContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiDoctorController(medidContext _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMDoctor> GetAllData()
        {
            List<VMDoctor> data = (from v in db.MDoctors
                                        join c in db.MBiodata on v.BiodataId equals c.Id
                                        where v.IsDelete == false 
                                        select new VMDoctor
                                        {
                                            Id = v.Id,
                                            Str = v.Str,
                                            CreatedBy = v.CreatedBy,
                                            CreatedOn = v.CreatedOn,
                                            ModifiedBy = v.ModifiedBy,
                                            ModifiedOn = v.ModifiedOn,
                                            DeletedBy = v.DeletedBy,
                                            DeletedOn = v.DeletedOn,
                                            IsDelete = v.IsDelete,

                                            BiodataId = v.BiodataId,
                                            Fullname = c.Fullname,
                                        }).ToList();
            return data;

        }
        [HttpGet("GetDataById/{id}")]
        public VMDoctor GetDataById(int id)
        {
            VMDoctor data = (from v in db.MDoctors
                            join c in db.MBiodata on v.BiodataId equals c.Id
                            where v.IsDelete == false && v.Id == id
                            select new VMDoctor
                            {
                                Id = v.Id,
                                Str = v.Str,
                                CreatedBy = v.CreatedBy,
                                CreatedOn = v.CreatedOn,
                                ModifiedBy = v.ModifiedBy,
                                ModifiedOn = v.ModifiedOn,
                                DeletedBy = v.DeletedBy,
                                DeletedOn = v.DeletedOn,
                                IsDelete = v.IsDelete,

                                BiodataId = v.BiodataId,
                                Fullname = c.Fullname,
                            }).FirstOrDefault()!;
            return data;
        }
        [HttpGet("GetDataByBiodataId/{id}")]
        public List<VMDoctor> GetDataByBiodataId(int id)
        {
            List<VMDoctor> data = (from v in db.MDoctors
                                   join c in db.MBiodata on v.BiodataId equals c.Id
                                   where v.IsDelete == false && v.BiodataId == id
                                   select new VMDoctor
                                   {
                                       Id = v.Id,
                                       Str = v.Str,
                                       CreatedBy = v.CreatedBy,
                                       CreatedOn = v.CreatedOn,
                                       ModifiedBy = v.ModifiedBy,
                                       ModifiedOn = v.ModifiedOn,
                                       DeletedBy = v.DeletedBy,
                                       DeletedOn = v.DeletedOn,
                                       IsDelete = v.IsDelete,

                                       BiodataId = v.BiodataId,
                                       Fullname = c.Fullname,
                                   }).ToList();
            return data;
        }
        [HttpPost("Save")]
        public VMResponse Save(MDoctor data)
        {
           
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            
            data.IsDelete = false;

            try
            {
                db.Add(data); db.SaveChanges();

                respon.Message = "Data succses saved";
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed Saved" + e.Message;
            }
            return respon;
        }
        [HttpPut("Edit")]
        public VMResponse Edit(MDoctor data)
        {
            MDoctor dta = db.MDoctors.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dta != null)
            {
                dta.Str = data.Str;
                dta.BiodataId = data.BiodataId;
                
                dta.ModifiedBy = IdUser;
                dta.ModifiedOn = DateTime.Now;
                
                dta.IsDelete = false;

                try
                {
                    db.Update(dta); db.SaveChanges();

                    respon.Message = "Data Succes Update";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Data Failed" + e.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }
            return respon;
        }

        [HttpDelete("Delete/{id}/{DeletedBy}")]
        public VMResponse Delete(int id, int DeletedBy)
        {
            MDoctor dta = db.MDoctors.Where(a => a.Id == id).FirstOrDefault()!;

            if (dta != null)
            {
                dta.IsDelete = true;
                
                dta.DeletedBy = IdUser;
                dta.DeletedOn = DateTime.Now;

                try
                {
                    db.Update(dta);
                    db.SaveChanges();

                    respon.Message = $"Data {dta.Str} success deleted";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Delete Failed" + e.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }
            return respon;
        }
        
        [HttpGet("GetDataByTindakanMedis/{id}")]
        public List<VMTindakanMedis> GetDataByTindakanMedis(int id)
        {
            List<VMTindakanMedis> data = (from tdm in db.TDoctorTreatments
                                          join doc in db.MDoctors on tdm.DoctorId equals doc.Id
                                          join bio in db.MBiodata on doc.BiodataId equals bio.Id
                                          where tdm.DoctorId == id
                                          select new VMTindakanMedis
                                          {
                                              Id = tdm.Id,
                                              NameTind = tdm.Name,
                                              DoctorId = tdm.DoctorId,
                                              CreateBy = tdm.CreateBy,
                                              CreateOn = tdm.CreateOn,
                                              ModifiedBy = tdm.ModifiedBy,
                                              ModifiedOn = tdm.ModifiedOn,
                                              DeleteBy = tdm.DeleteBy,
                                              DeleteOn = tdm.DeleteOn,
                                              IsDelete = tdm.IsDelete,

                                              Fullname = bio.Fullname,
                                              BiodataId = doc.BiodataId
                                          }).ToList();
            return data;
        }

        [HttpGet("GetDataByEducationDoctor/{id}")]
        public List<VMEducationDoctor> GetDataByEducationDoctor(int id)
        {
            List<VMEducationDoctor> data = (from edu in db.MDoctorEducations
                                            join doc in db.MDoctors on edu.DoctorId equals doc.Id
                                            join bio in db.MBiodata on doc.BiodataId equals bio.Id
                                            where edu.DoctorId == id
                                            select new VMEducationDoctor
                                            {
                                                Id = edu.Id,
                                                EducationLevelId = edu.EducationLevelId,
                                                InstitutionName = edu.InstitutionName,
                                                Major = edu.Major,
                                                StartYear = edu.StartYear,
                                                EndYear = edu.EndYear,
                                                IsLastEducation = edu.IsLastEducation,
                                                DoctorId = edu.DoctorId,
                                                CreatedBy = edu.CreatedBy,
                                                CreatedOn = edu.CreatedOn,
                                                ModifiedBy = edu.ModifiedBy,
                                                ModifiedOn = edu.ModifiedOn,
                                                DeletedBy = edu.DeletedBy,
                                                DeltedOn = edu.DeltedOn,
                                                IsDelete = edu.IsDelete,

                                                Fullname = bio.Fullname,
                                                BiodataId = doc.BiodataId
                                            }).ToList();
            return data;
        }
        [HttpGet("GetDataByRiwayatLocation/{id}")]
        public List<VMRiwayatLocation> GetDataByRiwayatLocation(int id)
        {
            List<VMRiwayatLocation> data = (from dsp in db.TCurrentDoctorSpecializations
                                            join spe in db.MSpecializations on dsp.SpecializationId equals spe.Id
                                            join doc in db.MDoctors on dsp.DoctorId equals doc.Id
                                            join bio in db.MBiodata on doc.BiodataId equals bio.Id
                                            join drsc in db.TDoctorOfficeSchedules on doc.Id equals drsc.DoctorId
                                            join mdfs in db.MMedicalFacilitySchedules on drsc.MedicalFacilityScheduleId equals mdfs.Id
                                            join mdfc in db.MMedicalFacilities on mdfs.MedicalFacilityId equals mdfc.Id
                                            join loc in db.MLocations on mdfc.LocationId equals loc.Id
                                            where doc.Id == id
                                            select new VMRiwayatLocation
                                            {
                                                Id = doc.Id,
                                                Fullname = bio.Fullname,
                                                NameSpesial = spe.Name,
                                                NameRs = mdfc.Name,
                                                NameLocation = loc.Name
                                                

                                                
                                            }).ToList();
            return data;
        }

        [HttpGet("GetDataByLocationPraktek/{id}")]
        public List<VMLocationPraktek> GetDataByLocationPraktek(int id)
        {
            List<VMLocationPraktek> data = (from dsp in db.TCurrentDoctorSpecializations
                                            join spe in db.MSpecializations on dsp.SpecializationId equals spe.Id
                                            join doc in db.MDoctors on dsp.DoctorId equals doc.Id
                                            join bio in db.MBiodata on doc.BiodataId equals bio.Id
                                            join drsc in db.TDoctorOfficeSchedules on doc.Id equals drsc.DoctorId
                                            join mdfs in db.MMedicalFacilitySchedules on drsc.MedicalFacilityScheduleId equals mdfs.Id
                                            join mdfc in db.MMedicalFacilities on mdfs.MedicalFacilityId equals mdfc.Id
                                            join medfac in db.MMedicalFacilityCategories on mdfc.MedicalFacilityCategoryId equals medfac.Id
                                            join docof in db.TDoctorOffices on doc.Id equals docof.DoctorId
                                            join docoftr in db.TDoctorOfficeTreatments on docof.Id equals docoftr.DoctorOfficeId
                                            join docoftrpri in db.TDoctorOfficeTreatmentPrices on docoftr.Id equals docoftrpri.DoctorOfficeTreatmentId
                                            where doc.Id == id
                                            select new VMLocationPraktek
                                            {
                                                Id = doc.Id,
                                                Fullname = bio.Fullname,
                                                NameRs = mdfc.Name,
                                                NamaPoli = medfac.Name,
                                                Hari = mdfs.Day,
                                                Mulai = mdfs.TimeScheduleStart,
                                                Selesai = mdfs.TimeScheduleEnd,
                                                HargaMulai = docoftrpri.PriceStartFrom.ToString()
                                            }).Distinct().ToList();
            return data;
        }
    }
}
